/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.datamodel;

/**
 *
 * @author deimios
 */
public class RaportInscriereDataModel {

    String unitate;
    Long total, curent, transfer, promovati, repetenti, corigenti, neincheiat;
    Long abandon, promoInscrisi, promoNeinscrisi, anterior, necompletat;

    public RaportInscriereDataModel() {
    }

    public RaportInscriereDataModel(String unitate, Long total, Long curent, Long transfer, Long promovati, Long repetenti, Long corigenti, Long neincheiat, Long abandon, Long promoInscrisi, Long promoNeinscrisi, Long anterior, Long necompletat) {
        this.unitate = unitate;
        this.total = total;
        this.curent = curent;
        this.transfer = transfer;
        this.promovati = promovati;
        this.repetenti = repetenti;
        this.corigenti = corigenti;
        this.neincheiat = neincheiat;
        this.abandon = abandon;
        this.promoInscrisi = promoInscrisi;
        this.promoNeinscrisi = promoNeinscrisi;
        this.anterior = anterior;
        this.necompletat = necompletat;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getCurent() {
        return curent;
    }

    public void setCurent(Long curent) {
        this.curent = curent;
    }

    public Long getTransfer() {
        return transfer;
    }

    public void setTransfer(Long transfer) {
        this.transfer = transfer;
    }

    public Long getPromovati() {
        return promovati;
    }

    public void setPromovati(Long promovati) {
        this.promovati = promovati;
    }

    public Long getRepetenti() {
        return repetenti;
    }

    public void setRepetenti(Long repetenti) {
        this.repetenti = repetenti;
    }

    public Long getCorigenti() {
        return corigenti;
    }

    public void setCorigenti(Long corigenti) {
        this.corigenti = corigenti;
    }

    public Long getNeincheiat() {
        return neincheiat;
    }

    public void setNeincheiat(Long neincheiat) {
        this.neincheiat = neincheiat;
    }

    public Long getAbandon() {
        return abandon;
    }

    public void setAbandon(Long abandon) {
        this.abandon = abandon;
    }

    public Long getPromoInscrisi() {
        return promoInscrisi;
    }

    public void setPromoInscrisi(Long promoInscrisi) {
        this.promoInscrisi = promoInscrisi;
    }

    public Long getPromoNeinscrisi() {
        return promoNeinscrisi;
    }

    public void setPromoNeinscrisi(Long promoNeinscrisi) {
        this.promoNeinscrisi = promoNeinscrisi;
    }

    public Long getAnterior() {
        return anterior;
    }

    public void setAnterior(Long anterior) {
        this.anterior = anterior;
    }

    public Long getNecompletat() {
        return necompletat;
    }

    public void setNecompletat(Long necompletat) {
        this.necompletat = necompletat;
    }

    public String getUnitate() {
        return unitate;
    }

    public void setUnitate(String unitate) {
        this.unitate = unitate;
    }
}
