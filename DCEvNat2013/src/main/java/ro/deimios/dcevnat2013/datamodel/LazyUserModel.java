package ro.deimios.dcevnat2013.datamodel;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Users;
import ro.deimios.dcevnat2013.facade.UsersFacadeLocal;

/**
 *
 * @author deimios
 */
public class LazyUserModel extends LazyDataModel<Users> {

    UsersFacadeLocal usersFacade = lookupUsersFacadeLocal();

       
    @Override
    public Users getRowData(String rowKey) {
        long uid = Long.valueOf(rowKey);
        return usersFacade.find(uid);
    }

    @Override
    public Object getRowKey(Users users) {
        return users.getId();
    }

    @Override
    public List<Users> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<Users> unitateList = usersFacade.findUnitateFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters);
        this.setRowCount((int) usersFacade.countFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters));
        return unitateList;
    }
    
    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }
    
    private UsersFacadeLocal lookupUsersFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (UsersFacadeLocal) c.lookup("java:global/DCEvNat/UsersFacade!ro.deimios.dcevnat2013.facade.UsersFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
