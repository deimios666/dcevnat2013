package ro.deimios.dcevnat2013.datamodel;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;

/**
 *
 * @author deimios
 */
public class LazyElevModel extends LazyDataModel<Elev> {

    ElevFacadeLocal elevFacade = lookupElevFacadeLocal();
    private int sirues;

    public LazyElevModel(int sirues) {
        this.sirues = sirues;
    }

    @Override
    public Elev getRowData(String rowKey) {
        long cnp = Long.valueOf(rowKey);
        return elevFacade.find(cnp);
    }

    @Override
    public Object getRowKey(Elev elev) {
        return elev.getCnp();
    }

    @Override
    public List<Elev> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (sirues != 458659) {
            filters.put("unitate.unitateSuperioara.sirues", String.valueOf(sirues));
        }
        List<Elev> elevList = elevFacade.findElevFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters);
        this.setRowCount((int) elevFacade.countFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters));
        return elevList;
    }

    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    private ElevFacadeLocal lookupElevFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ElevFacadeLocal) c.lookup("java:global/DCEvNat/ElevFacade!ro.deimios.dcevnat2013.facade.ElevFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
