package ro.deimios.dcevnat2013.datamodel;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.facade.UnitateFacadeLocal;

/**
 *
 * @author deimios
 */
public class LazyUnitateModel extends LazyDataModel<Unitate> {

    UnitateFacadeLocal unitateFacade = lookupUnitateFacadeLocal();

    @Override
    public Unitate getRowData(String rowKey) {
        long sirues = Long.valueOf(rowKey);
        return unitateFacade.find(sirues);
    }

    @Override
    public Object getRowKey(Unitate unitate) {
        return unitate.getSirues();
    }

    @Override
    public List<Unitate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<Unitate> unitateList = unitateFacade.findUnitateFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters);
        this.setRowCount((int) unitateFacade.countFilteredOrderedLimited(first, pageSize, sortField, sortOrder, filters));
        return unitateList;
    }
    
    @Override
    public void setRowIndex(int rowIndex) {
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    private UnitateFacadeLocal lookupUnitateFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (UnitateFacadeLocal) c.lookup("java:global/DCEvNat/UnitateFacade!ro.deimios.dcevnat2013.facade.UnitateFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
