package ro.deimios.dcevnat2013.validators;

import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author deimios
 */
@FacesValidator("notaValidator")
public class NotaValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        BigDecimal nota = (BigDecimal) value;
        BigDecimal minimum = new BigDecimal(0);
        BigDecimal maximum = new BigDecimal(10);
        if (nota.compareTo(minimum) >= 0 && nota.compareTo(maximum) <= 0) {
        } else {
            FacesMessage msg =
                    new FacesMessage("Format invalid",
                    "Nota trebuie să fie de forma 10.00");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
}
