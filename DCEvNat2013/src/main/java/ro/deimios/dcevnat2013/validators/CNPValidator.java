package ro.deimios.dcevnat2013.validators;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author deimios
 */
@FacesValidator("CNPValidator")
public class CNPValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //try {
            String cnp = (String) value;
            String errors = "";

            //check length
            if (cnp.length() != 13) {
                errors += "Lungime invalidă!\n";
                FacesMessage msg =
                        new FacesMessage("CNP invalid",
                        errors);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }

            //check sex/century
            String sexCentury = cnp.substring(0, 1);
            int sc = Integer.valueOf(sexCentury);
            String century = "";

            if (sc == 0) {
                errors += "Sexul nu poate fi 0\n";
                FacesMessage msg =
                        new FacesMessage("CNP invalid",
                        errors);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            } else if (sc < 3) {
                century = "19";
            } else if (sc < 5) {
                century = "18";
            } else if (sc < 7) {
                century = "20";
            }

            if (sc < 7) {
                //we only check the rest of the data for residents
                //check birthdate
                try {
                    DateFormat df = new SimpleDateFormat("yyyyMMdd");
                    df.setLenient(false);
                    df.parse(century + cnp.substring(1, 7));

                } catch (ParseException e) {
                    errors += "Data nașterii invalidă\n";
                    FacesMessage msg =
                            new FacesMessage("CNP invalid",
                            errors);
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }
                //check judet
                int judet = Integer.valueOf(cnp.substring(7, 9));
                if (!(judet > 0 && (judet < 47 || judet == 51 || judet == 52))) {
                    errors += "Cod județ invalid\n";
                    FacesMessage msg =
                            new FacesMessage("CNP invalid",
                            errors);
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }

            }

            //check checksum
            int[] checkSum = {2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9};
            int sum = 0;
            for (int i = 0; i < 12; i++) {
                sum = sum + (Integer.valueOf(cnp.substring(i, i + 1)) * checkSum[i]);
            }
            sum = sum % 11;
            if (sum == 10) {
                sum = 1;
            }
            if (Integer.valueOf(cnp.substring(12, 13)) != sum) {
                errors += "Erorare cod validare CNP";
                FacesMessage msg =
                        new FacesMessage("CNP invalid",
                        errors);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        /*} catch (Exception ex) {
            //any error results in a fail
            FacesMessage msg = new FacesMessage("CNP invalid", "");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }*/
    }
}
