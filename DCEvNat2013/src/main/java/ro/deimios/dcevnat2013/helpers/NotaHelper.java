package ro.deimios.dcevnat2013.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author deimios
 */
public class NotaHelper {

    public static final BigDecimal N950 = new BigDecimal("9.50");
    public static final BigDecimal N200 = new BigDecimal("2.00");
    public static final BigDecimal N300 = new BigDecimal("3.00");
    public static final BigDecimal N050 = new BigDecimal("0.50");

    public static BigDecimal contestat(BigDecimal ic, BigDecimal dc) {
        if (dc == null) {
            return ic;
        }

        if (ic.compareTo(dc) == 0) {
            return ic;
        }

        if (dc.compareTo(BigDecimal.ZERO) == 0) {
            return ic;
        }

        if (ic.compareTo(N950) < 0) {
            //if less than 9.50 then check point diff
            BigDecimal difference = ic.subtract(dc);
            if (difference.abs().compareTo(N050) < 0) {
                //difference is less then 0.50
                return ic;
            } else {
                return dc;
            }

        } else {
            return dc;
        }
    }

    public static BigDecimal average(BigDecimal rom, BigDecimal mag, BigDecimal mat) {
        BigDecimal sum = BigDecimal.ZERO;
        if (rom.compareTo(BigDecimal.ZERO) == 0 || mat.compareTo(BigDecimal.ZERO) == 0 || mag.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        } else {
            sum = sum.add(rom);
            sum = sum.add(mat);
            sum = sum.add(mag);
            sum = sum.divide(N300, 2, RoundingMode.DOWN);
        }
        return sum;
    }

    public static BigDecimal average(BigDecimal rom, BigDecimal mat) {
        BigDecimal sum = BigDecimal.ZERO;
        if (rom.compareTo(BigDecimal.ZERO) == 0 || mat.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        } else {
            sum = sum.add(rom);
            sum = sum.add(mat);
            sum = sum.divide(N200, 2, RoundingMode.DOWN);
        }
        return sum;
    }
}
