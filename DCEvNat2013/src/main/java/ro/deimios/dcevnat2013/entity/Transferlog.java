/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "transferlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transferlog.findAll", query = "SELECT t FROM Transferlog t"),
    @NamedQuery(name = "Transferlog.findById", query = "SELECT t FROM Transferlog t WHERE t.id = :id"),
    @NamedQuery(name = "Transferlog.findByEventname", query = "SELECT t FROM Transferlog t WHERE t.eventname = :eventname"),
    @NamedQuery(name = "Transferlog.findByEventTime", query = "SELECT t FROM Transferlog t WHERE t.eventTime = :eventTime")})
public class Transferlog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "eventname")
    private String eventname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventTime;
    @JoinColumn(name = "unitate_la", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate unitateLa;
    @JoinColumn(name = "unitate_dela", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate unitateDela;
    @JoinColumn(name = "elev", referencedColumnName = "cnp")
    @ManyToOne(optional = false)
    private Elev elev;

    public Transferlog() {
    }

    public Transferlog(Integer id) {
        this.id = id;
    }

    public Transferlog(Integer id, String eventname, Date eventTime) {
        this.id = id;
        this.eventname = eventname;
        this.eventTime = eventTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public Unitate getUnitateLa() {
        return unitateLa;
    }

    public void setUnitateLa(Unitate unitateLa) {
        this.unitateLa = unitateLa;
    }

    public Unitate getUnitateDela() {
        return unitateDela;
    }

    public void setUnitateDela(Unitate unitateDela) {
        this.unitateDela = unitateDela;
    }

    public Elev getElev() {
        return elev;
    }

    public void setElev(Elev elev) {
        this.elev = elev;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transferlog)) {
            return false;
        }
        Transferlog other = (Transferlog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Transferlog[ id=" + id + " ]";
    }
}
