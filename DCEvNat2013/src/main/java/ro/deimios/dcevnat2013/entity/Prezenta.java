/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "prezenta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prezenta.findAll", query = "SELECT p FROM Prezenta p"),
    @NamedQuery(name = "Prezenta.findById", query = "SELECT p FROM Prezenta p WHERE p.id = :id")})
public class Prezenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "final", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StarePrezenta final1;
    @JoinColumn(name = "mat", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StarePrezenta mat;
    @JoinColumn(name = "mag", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StarePrezenta mag;
    @JoinColumn(name = "rom", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private StarePrezenta rom;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "prezentaId")
    private List<Elev> elevList;

    public Prezenta() {
    }

    public Prezenta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StarePrezenta getFinal1() {
        return final1;
    }

    public void setFinal1(StarePrezenta final1) {
        this.final1 = final1;
    }

    public StarePrezenta getMat() {
        return mat;
    }

    public void setMat(StarePrezenta mat) {
        this.mat = mat;
    }

    public StarePrezenta getMag() {
        return mag;
    }

    public void setMag(StarePrezenta mag) {
        this.mag = mag;
    }

    public StarePrezenta getRom() {
        return rom;
    }

    public void setRom(StarePrezenta rom) {
        this.rom = rom;
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prezenta)) {
            return false;
        }
        Prezenta other = (Prezenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Prezenta[ id=" + id + " ]";
    }
}
