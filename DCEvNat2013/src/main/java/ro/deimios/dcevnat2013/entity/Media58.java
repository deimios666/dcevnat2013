/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "media58")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Media58.findAll", query = "SELECT m FROM Media58 m"),
    @NamedQuery(name = "Media58.findById", query = "SELECT m FROM Media58 m WHERE m.id = :id"),
    @NamedQuery(name = "Media58.findByMedia", query = "SELECT m FROM Media58 m WHERE m.media = :media")})
public class Media58 implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "inscris")
    private boolean inscris;
    @JoinColumn(name = "stare_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Stare stareId;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "media")
    private BigDecimal media;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "media58Id")
    private List<Elev> elevList;

    public Media58() {
    }

    public Media58(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMedia() {
        return media;
    }

    public void setMedia(BigDecimal media) {
        this.media = media;
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Media58)) {
            return false;
        }
        Media58 other = (Media58) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Media58[ id=" + id + " ]";
    }

    public Stare getStareId() {
        return stareId;
    }

    public void setStareId(Stare stareId) {
        this.stareId = stareId;
    }

    public boolean getInscris() {
        return inscris;
    }

    public void setInscris(boolean inscris) {
        this.inscris = inscris;
    }
    
}
