package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "elev")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Elev.findAll", query = "SELECT e FROM Elev e"),
    @NamedQuery(name = "Elev.findByCnp", query = "SELECT e FROM Elev e WHERE e.cnp = :cnp"),
    @NamedQuery(name = "Elev.findByNume", query = "SELECT e FROM Elev e WHERE e.nume = :nume"),
    @NamedQuery(name = "Elev.findByIni", query = "SELECT e FROM Elev e WHERE e.ini = :ini"),
    @NamedQuery(name = "Elev.findByCZE", query = "SELECT e FROM Elev e WHERE e.unitate.unitateSuperioara.centruEvaluare.sirues = :sirues and e.media58Id.inscris = 1 order by e.nume, e.prenume, e.ini"),
    @NamedQuery(name = "Elev.findByPJ", query = "SELECT e FROM Elev e WHERE e.unitate.unitateSuperioara.sirues = :sirues and e.media58Id.inscris = 1 order by e.nume, e.prenume, e.ini"),
    @NamedQuery(name = "Elev.findByPrenume", query = "SELECT e FROM Elev e WHERE e.prenume = :prenume")})
public class Elev implements Serializable {

    @JoinColumn(name = "prezenta_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Prezenta prezentaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elev")
    private List<Transferlog> transferlogList;
    @JoinColumn(name = "unitate", referencedColumnName = "sirues")
    @ManyToOne(optional = false)
    private Unitate unitate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cnp")
    private Long cnp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "nume")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "ini")
    private String ini;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "prenume")
    private String prenume;
    @JoinColumn(name = "media58_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Media58 media58Id;
    @JoinColumn(name = "mediu_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Mediu mediuId;
    @JoinColumn(name = "nota_dc_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private NotaDc notaDcId;
    @JoinColumn(name = "nota_ic_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private NotaIc notaIcId;
    @JoinColumn(name = "nota_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Nota notaId;
    @JoinColumn(name = "sectia_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sectia sectiaId;
    @JoinColumn(name = "seria_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Seria seriaId;

    public Elev() {
    }

    public Elev(Long cnp) {
        this.cnp = cnp;
    }

    public Elev(Long cnp, String nume, String ini, String prenume) {
        this.cnp = cnp;
        this.nume = nume;
        this.ini = ini;
        this.prenume = prenume;
    }

    public Long getCnp() {
        return cnp;
    }

    public void setCnp(Long cnp) {
        this.cnp = cnp;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getIni() {
        return ini;
    }

    public void setIni(String ini) {
        this.ini = ini;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public Media58 getMedia58Id() {
        return media58Id;
    }

    public void setMedia58Id(Media58 media58Id) {
        this.media58Id = media58Id;
    }

    public Mediu getMediuId() {
        return mediuId;
    }

    public void setMediuId(Mediu mediuId) {
        this.mediuId = mediuId;
    }

    public NotaDc getNotaDcId() {
        return notaDcId;
    }

    public void setNotaDcId(NotaDc notaDcId) {
        this.notaDcId = notaDcId;
    }

    public NotaIc getNotaIcId() {
        return notaIcId;
    }

    public void setNotaIcId(NotaIc notaIcId) {
        this.notaIcId = notaIcId;
    }

    public Nota getNotaId() {
        return notaId;
    }

    public void setNotaId(Nota notaId) {
        this.notaId = notaId;
    }

    public Sectia getSectiaId() {
        return sectiaId;
    }

    public void setSectiaId(Sectia sectiaId) {
        this.sectiaId = sectiaId;
    }

    public Seria getSeriaId() {
        return seriaId;
    }

    public void setSeriaId(Seria seriaId) {
        this.seriaId = seriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cnp != null ? cnp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elev)) {
            return false;
        }
        Elev other = (Elev) object;
        if ((this.cnp == null && other.cnp != null) || (this.cnp != null && !this.cnp.equals(other.cnp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Elev[ cnp=" + cnp + " ]";
    }

    public Unitate getUnitate() {
        return unitate;
    }

    public void setUnitate(Unitate unitate) {
        this.unitate = unitate;
    }

    @XmlTransient
    public List<Transferlog> getTransferlogList() {
        return transferlogList;
    }

    public void setTransferlogList(List<Transferlog> transferlogList) {
        this.transferlogList = transferlogList;
    }

    public Prezenta getPrezentaId() {
        return prezentaId;
    }

    public void setPrezentaId(Prezenta prezentaId) {
        this.prezentaId = prezentaId;
    }
}
