/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "stare_prezenta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StarePrezenta.findAll", query = "SELECT s FROM StarePrezenta s"),
    @NamedQuery(name = "StarePrezenta.findById", query = "SELECT s FROM StarePrezenta s WHERE s.id = :id"),
    @NamedQuery(name = "StarePrezenta.findByNume", query = "SELECT s FROM StarePrezenta s WHERE s.nume = :nume")})
public class StarePrezenta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nume")
    private String nume;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "final1")
    private List<Prezenta> prezentaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mat")
    private List<Prezenta> prezentaList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mag")
    private List<Prezenta> prezentaList2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rom")
    private List<Prezenta> prezentaList3;

    public StarePrezenta() {
    }

    public StarePrezenta(Integer id) {
        this.id = id;
    }

    public StarePrezenta(Integer id, String nume) {
        this.id = id;
        this.nume = nume;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @XmlTransient
    public List<Prezenta> getPrezentaList() {
        return prezentaList;
    }

    public void setPrezentaList(List<Prezenta> prezentaList) {
        this.prezentaList = prezentaList;
    }

    @XmlTransient
    public List<Prezenta> getPrezentaList1() {
        return prezentaList1;
    }

    public void setPrezentaList1(List<Prezenta> prezentaList1) {
        this.prezentaList1 = prezentaList1;
    }

    @XmlTransient
    public List<Prezenta> getPrezentaList2() {
        return prezentaList2;
    }

    public void setPrezentaList2(List<Prezenta> prezentaList2) {
        this.prezentaList2 = prezentaList2;
    }

    @XmlTransient
    public List<Prezenta> getPrezentaList3() {
        return prezentaList3;
    }

    public void setPrezentaList3(List<Prezenta> prezentaList3) {
        this.prezentaList3 = prezentaList3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StarePrezenta)) {
            return false;
        }
        StarePrezenta other = (StarePrezenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.StarePrezenta[ id=" + id + " ]";
    }
    
}
