package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "unitate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Unitate.findAll", query = "SELECT u FROM Unitate u order by u.nume"),
    @NamedQuery(name = "Unitate.findBySirues", query = "SELECT u FROM Unitate u WHERE u.sirues = :sirues"),
    @NamedQuery(name = "Unitate.findBySiruesSuperior", query = "SELECT u FROM Unitate u WHERE u.unitateSuperioara.sirues = :sirues order by u.nume"),
    @NamedQuery(name = "Unitate.findPJ", query = "SELECT u FROM Unitate u WHERE u.isPj = true AND u.isCentruEvaluare = false ORDER BY u.nume"),
    @NamedQuery(name = "Unitate.findPJByCZE", query = "SELECT u FROM Unitate u WHERE u.unitateSuperioara.centruEvaluare.sirues = :sirues AND u.isPj = true order by u.nume"),
    @NamedQuery(name = "Unitate.searchBySiruesSuperiorNume", query = "SELECT u FROM Unitate u WHERE u.unitateSuperioara.sirues = :sirues AND u.nume LIKE :nume order by u.nume"),
    @NamedQuery(name = "Unitate.findByNume", query = "SELECT u FROM Unitate u WHERE u.nume = :nume"),
    @NamedQuery(name = "Unitate.searchByNumeOrdered", query = "SELECT u FROM Unitate u WHERE u.nume LIKE :nume ORDER BY u.nume"),
    @NamedQuery(name = "Unitate.findByIsCentruComunicare", query = "SELECT u FROM Unitate u WHERE u.isCentruComunicare = :isCentruComunicare"),
    @NamedQuery(name = "Unitate.findByIsCentruEvaluare", query = "SELECT u FROM Unitate u WHERE u.isCentruEvaluare = :isCentruEvaluare"),
    @NamedQuery(name = "Unitate.findByIsPj", query = "SELECT u FROM Unitate u WHERE u.isPj = :isPj")})
public class Unitate implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitate")
    private List<Users> usersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitateLa")
    private List<Transferlog> transferlogList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitateDela")
    private List<Transferlog> transferlogList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitateSirues")
    private List<Notifications> notificationsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitate")
    private List<Elev> elevList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "sirues")
    private Integer sirues;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nume")
    private String nume;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_centru_comunicare")
    private boolean isCentruComunicare;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_centru_evaluare")
    private boolean isCentruEvaluare;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_centru_contestatii")
    private boolean isCentruContestatii;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_pj")
    private boolean isPj;
    @OneToMany(mappedBy = "centruEvaluare")
    private List<Unitate> centruEvaluareArondatList;
    @JoinColumn(name = "centru_evaluare", referencedColumnName = "sirues")
    @ManyToOne
    private Unitate centruEvaluare;
    @OneToMany(mappedBy = "centruComunicare")
    private List<Unitate> centruComunicareArondatList;
    @JoinColumn(name = "centru_comunicare", referencedColumnName = "sirues")
    @ManyToOne
    private Unitate centruComunicare;
    @OneToMany(mappedBy = "centruContestatii")
    private List<Unitate> centruContestatiiArondatList;
    @JoinColumn(name = "centru_contestatii", referencedColumnName = "sirues")
    @ManyToOne
    private Unitate centruContestatii;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitateSuperioara")
    private List<Unitate> unitateArondatList;
    @JoinColumn(name = "unitate_superioara", referencedColumnName = "sirues")
    @ManyToOne(optional = true)
    private Unitate unitateSuperioara;

    public Unitate() {
    }

    public Unitate(Integer sirues) {
        this.sirues = sirues;
    }

    public Unitate(Integer sirues, String nume, boolean isCentruComunicare, boolean isCentruEvaluare, boolean isPj) {
        this.sirues = sirues;
        this.nume = nume;
        this.isCentruComunicare = isCentruComunicare;
        this.isCentruEvaluare = isCentruEvaluare;
        this.isPj = isPj;
    }

    public boolean isIsCentruContestatii() {
        return isCentruContestatii;
    }

    public void setIsCentruContestatii(boolean isCentruContestatii) {
        this.isCentruContestatii = isCentruContestatii;
    }

    public Integer getSirues() {
        return sirues;
    }

    public void setSirues(Integer sirues) {
        this.sirues = sirues;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public boolean getIsCentruComunicare() {
        return isCentruComunicare;
    }

    public void setIsCentruComunicare(boolean isCentruComunicare) {
        this.isCentruComunicare = isCentruComunicare;
    }

    public boolean getIsCentruEvaluare() {
        return isCentruEvaluare;
    }

    public void setIsCentruEvaluare(boolean isCentruEvaluare) {
        this.isCentruEvaluare = isCentruEvaluare;
    }

    public boolean getIsPj() {
        return isPj;
    }

    public void setIsPj(boolean isPj) {
        this.isPj = isPj;
    }

    @XmlTransient
    public List<Unitate> getCentruEvaluareArondatList() {
        return centruEvaluareArondatList;
    }

    public void setCentruEvaluareArondatList(List<Unitate> centruEvaluareArondatList) {
        this.centruEvaluareArondatList = centruEvaluareArondatList;
    }

    public Unitate getCentruEvaluare() {
        return centruEvaluare;
    }

    public void setCentruEvaluare(Unitate centruEvaluare) {
        this.centruEvaluare = centruEvaluare;
    }

    @XmlTransient
    public List<Unitate> getCentruComunicareArondatList() {
        return centruComunicareArondatList;
    }

    public void setCentruComunicareArondatList(List<Unitate> centruComunicareArondatList) {
        this.centruComunicareArondatList = centruComunicareArondatList;
    }

    public Unitate getCentruComunicare() {
        return centruComunicare;
    }

    public void setCentruComunicare(Unitate centruComunicare) {
        this.centruComunicare = centruComunicare;
    }

    @XmlTransient
    public List<Unitate> getUnitateArondatList() {
        return unitateArondatList;
    }

    public void setUnitateArondatList(List<Unitate> unitateArondatList) {
        this.unitateArondatList = unitateArondatList;
    }

    public Unitate getUnitateSuperioara() {
        return unitateSuperioara;
    }

    public void setUnitateSuperioara(Unitate unitateSuperioara) {
        this.unitateSuperioara = unitateSuperioara;
    }

    public Unitate getCentruContestatii() {
        return centruContestatii;
    }

    public void setCentruContestatii(Unitate centruContestatii) {
        this.centruContestatii = centruContestatii;
    }

    public List<Unitate> getCentruContestatiiArondatList() {
        return centruContestatiiArondatList;
    }

    public void setCentruContestatiiArondatList(List<Unitate> centruContestatiiArondatList) {
        this.centruContestatiiArondatList = centruContestatiiArondatList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sirues != null ? sirues.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unitate)) {
            return false;
        }
        Unitate other = (Unitate) object;
        if ((this.sirues == null && other.sirues != null) || (this.sirues != null && !this.sirues.equals(other.sirues))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Unitate[ sirues=" + sirues + " ]";
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @XmlTransient
    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @XmlTransient
    public List<Transferlog> getTransferlogList() {
        return transferlogList;
    }

    public void setTransferlogList(List<Transferlog> transferlogList) {
        this.transferlogList = transferlogList;
    }

    @XmlTransient
    public List<Transferlog> getTransferlogList1() {
        return transferlogList1;
    }

    public void setTransferlogList1(List<Transferlog> transferlogList1) {
        this.transferlogList1 = transferlogList1;
    }

    @XmlTransient
    public List<Notifications> getNotificationsList() {
        return notificationsList;
    }

    public void setNotificationsList(List<Notifications> notificationsList) {
        this.notificationsList = notificationsList;
    }

}
