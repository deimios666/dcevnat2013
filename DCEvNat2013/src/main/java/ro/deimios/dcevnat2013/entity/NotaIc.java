/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "nota_ic")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaIc.findAll", query = "SELECT n FROM NotaIc n"),
    @NamedQuery(name = "NotaIc.findById", query = "SELECT n FROM NotaIc n WHERE n.id = :id"),
    @NamedQuery(name = "NotaIc.findByRom", query = "SELECT n FROM NotaIc n WHERE n.rom = :rom"),
    @NamedQuery(name = "NotaIc.findByMag", query = "SELECT n FROM NotaIc n WHERE n.mag = :mag"),
    @NamedQuery(name = "NotaIc.findByMat", query = "SELECT n FROM NotaIc n WHERE n.mat = :mat")})
public class NotaIc implements Serializable {
    @Column(name = "media")
    private BigDecimal media;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "rom")
    private BigDecimal rom;
    @Column(name = "mag")
    private BigDecimal mag;
    @Column(name = "mat")
    private BigDecimal mat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notaIcId")
    private List<Elev> elevList;

    public NotaIc() {
    }

    public NotaIc(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getRom() {
        return rom;
    }

    public void setRom(BigDecimal rom) {
        this.rom = rom;
    }

    public BigDecimal getMag() {
        return mag;
    }

    public void setMag(BigDecimal mag) {
        this.mag = mag;
    }

    public BigDecimal getMat() {
        return mat;
    }

    public void setMat(BigDecimal mat) {
        this.mat = mat;
    }

    @XmlTransient
    public List<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(List<Elev> elevList) {
        this.elevList = elevList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaIc)) {
            return false;
        }
        NotaIc other = (NotaIc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.NotaIc[ id=" + id + " ]";
    }

    public BigDecimal getMedia() {
        return media;
    }

    public void setMedia(BigDecimal media) {
        this.media = media;
    }
    
}
