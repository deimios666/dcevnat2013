/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deimios
 */
@Entity
@Table(name = "notifications")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notifications.findAll", query = "SELECT n FROM Notifications n"),
    @NamedQuery(name = "Notifications.findById", query = "SELECT n FROM Notifications n WHERE n.id = :id"),
    @NamedQuery(name = "Notifications.findByNotification", query = "SELECT n FROM Notifications n WHERE n.notification = :notification"),
    @NamedQuery(name = "Notifications.findByNotificationDate", query = "SELECT n FROM Notifications n WHERE n.notificationDate = :notificationDate"),
    @NamedQuery(name = "Notifications.findBySirues", query = "SELECT n FROM Notifications n WHERE n.unitateSirues.sirues = :sirues order by n.id DESC"),
    @NamedQuery(name = "Notifications.findByNotificationNew", query = "SELECT n FROM Notifications n WHERE n.notificationNew = :notificationNew")})
public class Notifications implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2048)
    @Column(name = "notification")
    private String notification;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notification_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificationDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notification_new")
    private boolean notificationNew;
    @JoinColumn(name = "unitate_sirues", referencedColumnName = "sirues")
    @ManyToOne(optional = true)
    private Unitate unitateSirues;

    public Notifications() {
    }

    public Notifications(Integer id) {
        this.id = id;
    }

    public Notifications(Integer id, String notification, Date notificationDate, boolean notificationNew) {
        this.id = id;
        this.notification = notification;
        this.notificationDate = notificationDate;
        this.notificationNew = notificationNew;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Date getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    public boolean getNotificationNew() {
        return notificationNew;
    }

    public void setNotificationNew(boolean notificationNew) {
        this.notificationNew = notificationNew;
    }

    public Unitate getUnitateSirues() {
        return unitateSirues;
    }

    public void setUnitateSirues(Unitate unitateSirues) {
        this.unitateSirues = unitateSirues;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notifications)) {
            return false;
        }
        Notifications other = (Notifications) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ro.deimios.dcevnat2013.entity.Notifications[ id=" + id + " ]";
    }
    
}
