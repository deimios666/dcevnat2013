package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.entity.Nota;
import ro.deimios.dcevnat2013.entity.NotaDc;
import ro.deimios.dcevnat2013.entity.NotaIc;
import ro.deimios.dcevnat2013.facade.NotaDcFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaIcFacadeLocal;
import ro.deimios.dcevnat2013.helpers.NotaHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class NotaICEditorBean implements Serializable {

    @EJB
    private NotaDcFacadeLocal notaDcFacade;
    @EJB
    private NotaIcFacadeLocal notaIcFacade;
    @EJB
    private NotaFacadeLocal notaFacade;

    /**
     * Creates a new instance of PrezentaEditorBean
     */
    public NotaICEditorBean() {
    }

    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Elev elev = (Elev) source.getRowData();
        Nota nota = elev.getNotaId();
        NotaIc nota_ic = elev.getNotaIcId();
        NotaDc nota_dc = elev.getNotaDcId();

        if (nota_dc.getRom() == BigDecimal.ZERO) {
            nota_dc.setRom(nota_ic.getRom());
        }

        if (nota_dc.getMag() == BigDecimal.ZERO) {
            nota_dc.setMag(nota_ic.getMag());
        }

        if (nota_dc.getMat() == BigDecimal.ZERO) {
            nota_dc.setMat(nota_ic.getMat());
        }

        nota.setRom(NotaHelper.contestat(nota_ic.getRom(), nota_dc.getRom()));
        nota.setMag(NotaHelper.contestat(nota_ic.getMag(), nota_dc.getMag()));
        nota.setMat(NotaHelper.contestat(nota_ic.getMat(), nota_dc.getMat()));

        if (elev.getPrezentaId().getFinal1().getId() == 1) {
            if (elev.getSectiaId().getId() == 1) {
                //maghiara
                nota_ic.setMedia(NotaHelper.average(nota_ic.getRom(), nota_ic.getMag(), nota_ic.getMat()));
                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMag(), nota_dc.getMat()));
                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMag(), nota.getMat()));
            } else {
                nota_ic.setMedia(NotaHelper.average(nota_ic.getRom(), nota_ic.getMat()));
                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMat()));
                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMat()));
            }
        } else {
            nota_ic.setMedia(BigDecimal.ZERO);
            nota_dc.setMedia(BigDecimal.ZERO);
            nota.setMedia(BigDecimal.ZERO);
        }

        notaFacade.edit(nota);
        notaDcFacade.edit(nota_dc);
        notaIcFacade.edit(nota_ic);
    }
}
