package ro.deimios.dcevnat2013.beans;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.entity.Media58;
import ro.deimios.dcevnat2013.entity.Nota;
import ro.deimios.dcevnat2013.entity.NotaDc;
import ro.deimios.dcevnat2013.entity.NotaIc;
import ro.deimios.dcevnat2013.entity.Prezenta;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class ExportBean {

    @EJB
    private ElevFacadeLocal elevFacade;

    /**
     * Creates a new instance of ExportBean
     */
    public ExportBean() {
    }

    double ifnull(BigDecimal obj) {
        if (obj == null) {
            return 0;
        } else {
            return obj.doubleValue();
        }
    }

    public void getDBDump() {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 1;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "export_date.xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/export_date.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            DataFormat format = template.createDataFormat();
            style = template.createCellStyle();
            style.setDataFormat(format.getFormat("#0.00"));

            List<Elev> elevList = elevFacade.findAll();
            mainSheet.shiftRows(headerOffset, headerOffset, headerOffset);

            for (int i = 0; i < elevList.size(); i++) {
                Elev elev = elevList.get(i);
                Media58 media58 = elev.getMedia58Id();
                Prezenta prezenta = elev.getPrezentaId();
                Nota nota = elev.getNotaId();
                NotaIc nota_ic = elev.getNotaIcId();
                NotaDc nota_dc = elev.getNotaDcId();

                Row currentRow = mainSheet.getRow(i + headerOffset);
                if (currentRow == null) {
                    currentRow = mainSheet.createRow(i + headerOffset);
                }
                for (int j = 0; j < 30; j++) {
                    Cell newCell = currentRow.createCell(j);
                }
                //Unitate superioară
                currentRow.getCell(0).setCellValue(elev.getUnitate().getUnitateSuperioara().getNume());
                //Structura arondată
                currentRow.getCell(1).setCellValue(elev.getUnitate().getNume());
                //Centru Comunicare
                currentRow.getCell(2).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruComunicare().getNume());
                //Centru Evaluare
                currentRow.getCell(3).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruEvaluare().getNume());
                //CNP
                currentRow.createCell(4, Cell.CELL_TYPE_NUMERIC);
                currentRow.getCell(4).setCellValue(elev.getCnp());
                //Nume
                currentRow.getCell(5).setCellValue(elev.getNume());
                //Ini
                currentRow.getCell(6).setCellValue(elev.getIni());
                //Prenume
                currentRow.getCell(7).setCellValue(elev.getPrenume());
                //Seria
                currentRow.getCell(8).setCellValue(elev.getSeriaId().getNume());
                //Secția
                currentRow.getCell(9).setCellValue(elev.getSectiaId().getNume());
                //Mediul
                currentRow.getCell(10).setCellValue(elev.getMediuId().getNume());
                //Stare clasa VIII
                currentRow.getCell(11).setCellValue(media58.getStareId().getNume());
                //Media V-VIII
                currentRow.createCell(12, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(12).setCellValue(ifnull(media58.getMedia()));
                //Înscris la EN
                if (media58.getInscris()) {
                    currentRow.getCell(13).setCellValue("Da");
                } else {
                    currentRow.getCell(13).setCellValue("Nu");
                }
                //Stare ROM
                currentRow.getCell(14).setCellValue(prezenta.getRom().getNume());
                //Nota ROM
                currentRow.createCell(15, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(15).setCellValue(ifnull(nota.getRom()));
                //Nota ROM IC
                currentRow.createCell(16, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(16).setCellValue(ifnull(nota_ic.getRom()));
                //Nota ROM DC
                currentRow.createCell(17, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(17).setCellValue(ifnull(nota_dc.getRom()));
                //Stare MAG
                if (elev.getSectiaId().getId() == 1) {
                    currentRow.getCell(18).setCellValue(prezenta.getMag().getNume());
                    //Nota MAG
                    currentRow.createCell(19, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(19).setCellValue(ifnull(nota.getMag()));
                    //Nota MAG IC
                    currentRow.createCell(20, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(20).setCellValue(ifnull(nota_ic.getMag()));
                    //Nota MAG DC
                    currentRow.createCell(21, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(21).setCellValue(ifnull(nota_dc.getMag()));
                } else {
                    currentRow.getCell(18).setCellValue("");
                    currentRow.getCell(19).setCellValue("");
                    currentRow.getCell(20).setCellValue("");
                    currentRow.getCell(21).setCellValue("");
                }
                //Stare MAT
                currentRow.getCell(22).setCellValue(prezenta.getMat().getNume());
                //Nota MAT
                currentRow.createCell(23, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(23).setCellValue(ifnull(nota.getMat()));
                //Nota MAT IC
                currentRow.createCell(24, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(24).setCellValue(ifnull(nota_ic.getMat()));
                //Nota MAT DC
                currentRow.createCell(25, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(25).setCellValue(ifnull(nota_dc.getMat()));
                //Stare FINAL
                currentRow.getCell(26).setCellValue(prezenta.getFinal1().getNume());
                //Nota FINAL
                currentRow.createCell(27, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(27).setCellValue(ifnull(nota.getMedia()));
                //Nota FINAL IC
                currentRow.createCell(28, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(28).setCellValue(ifnull(nota_ic.getMedia()));
                //Nota FINAL DC
                currentRow.createCell(29, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                currentRow.getCell(29).setCellValue(ifnull(nota_dc.getMedia()));
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getNotaIcMacheta() {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 1;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "machetaNoteIc.xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/machetaNoteIc.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            DataFormat format = template.createDataFormat();
            style = template.createCellStyle();
            style.setDataFormat(format.getFormat("#0.00"));

            List<Elev> elevList = elevFacade.findByCZE(userBean.getSirues());
            mainSheet.shiftRows(headerOffset, headerOffset, headerOffset);

            for (int i = 0; i < elevList.size(); i++) {
                Elev elev = elevList.get(i);
                Prezenta prezenta = elev.getPrezentaId();
                NotaIc nota_ic = elev.getNotaIcId();
                Row currentRow = mainSheet.getRow(i + headerOffset);
                if (currentRow == null) {
                    currentRow = mainSheet.createRow(i + headerOffset);
                }
                for (int j = 0; j < 15; j++) {
                    currentRow.createCell(j);
                }
                //Unitate superioară
                currentRow.getCell(0).setCellValue(elev.getUnitate().getUnitateSuperioara().getNume());
                //Structura arondată
                currentRow.getCell(1).setCellValue(elev.getUnitate().getNume());
                //Centru Comunicare
                currentRow.getCell(2).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruComunicare().getNume());
                //Centru Evaluare
                currentRow.getCell(3).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruEvaluare().getNume());
                //CNP
                currentRow.createCell(4, Cell.CELL_TYPE_NUMERIC);
                currentRow.getCell(4).setCellValue(elev.getCnp());
                //Nume
                currentRow.getCell(5).setCellValue(elev.getNume());
                //Ini
                currentRow.getCell(6).setCellValue(elev.getIni());
                //Prenume
                currentRow.getCell(7).setCellValue(elev.getPrenume());
                //Seria
                currentRow.getCell(8).setCellValue(elev.getSeriaId().getNume());
                //Secția
                currentRow.getCell(9).setCellValue(elev.getSectiaId().getNume());
                //Mediul
                currentRow.getCell(10).setCellValue(elev.getMediuId().getNume());

                if (prezenta.getRom().getId() > 1) {
                    currentRow.getCell(11).setCellValue(prezenta.getRom().getNume());
                } else {
                    currentRow.createCell(11, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(11).setCellValue(ifnull(nota_ic.getRom()));
                }

                //Stare MAG
                if (elev.getSectiaId().getId() == 1) {
                    if (prezenta.getMag().getId() > 1) {
                        currentRow.getCell(12).setCellValue(prezenta.getMag().getNume());
                    } else {
                        //Nota MAG IC
                        currentRow.createCell(12, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                        currentRow.getCell(12).setCellValue(ifnull(nota_ic.getMag()));
                    }
                } else {
                    currentRow.getCell(12).setCellValue("");
                }

                if (prezenta.getMat().getId() > 1) {
                    currentRow.getCell(13).setCellValue(prezenta.getMat().getNume());
                } else {
                    currentRow.createCell(13, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(13).setCellValue(ifnull(nota_ic.getMat()));
                }

                if (prezenta.getFinal1().getId() > 1) {
                    currentRow.getCell(14).setCellValue(prezenta.getFinal1().getNume());
                } else {
                    currentRow.createCell(14, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(14).setCellValue(ifnull(nota_ic.getMedia()));
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getNotaDcMacheta() {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 1;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "machetaNoteDc.xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/machetaNoteDc.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            DataFormat format = template.createDataFormat();
            style = template.createCellStyle();
            style.setDataFormat(format.getFormat("#0.00"));

            List<Elev> elevList = elevFacade.findByCZE(userBean.getSirues());
            mainSheet.shiftRows(headerOffset, headerOffset, headerOffset);

            for (int i = 0; i < elevList.size(); i++) {
                Elev elev = elevList.get(i);
                Prezenta prezenta = elev.getPrezentaId();
                NotaDc nota_dc = elev.getNotaDcId();
                Row currentRow = mainSheet.getRow(i + headerOffset);
                if (currentRow == null) {
                    currentRow = mainSheet.createRow(i + headerOffset);
                }
                for (int j = 0; j < 15; j++) {
                    currentRow.createCell(j);
                }
                //Unitate superioară
                currentRow.getCell(0).setCellValue(elev.getUnitate().getUnitateSuperioara().getNume());
                //Structura arondată
                currentRow.getCell(1).setCellValue(elev.getUnitate().getNume());
                //Centru Comunicare
                currentRow.getCell(2).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruComunicare().getNume());
                //Centru Evaluare
                currentRow.getCell(3).setCellValue(elev.getUnitate().getUnitateSuperioara().getCentruEvaluare().getNume());
                //CNP
                currentRow.createCell(4, Cell.CELL_TYPE_NUMERIC);
                currentRow.getCell(4).setCellValue(elev.getCnp());
                //Nume
                currentRow.getCell(5).setCellValue(elev.getNume());
                //Ini
                currentRow.getCell(6).setCellValue(elev.getIni());
                //Prenume
                currentRow.getCell(7).setCellValue(elev.getPrenume());
                //Seria
                currentRow.getCell(8).setCellValue(elev.getSeriaId().getNume());
                //Secția
                currentRow.getCell(9).setCellValue(elev.getSectiaId().getNume());
                //Mediul
                currentRow.getCell(10).setCellValue(elev.getMediuId().getNume());

                if (prezenta.getRom().getId() > 1) {
                    currentRow.getCell(11).setCellValue(prezenta.getRom().getNume());
                } else {
                    currentRow.createCell(11, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(11).setCellValue(ifnull(nota_dc.getRom()));
                }

                //Stare MAG
                if (elev.getSectiaId().getId() == 1) {
                    if (prezenta.getMag().getId() > 1) {
                        currentRow.getCell(12).setCellValue(prezenta.getMag().getNume());
                    } else {
                        //Nota MAG IC
                        currentRow.createCell(12, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                        currentRow.getCell(12).setCellValue(ifnull(nota_dc.getMag()));
                    }
                } else {
                    currentRow.getCell(12).setCellValue("");
                }

                if (prezenta.getMat().getId() > 1) {
                    currentRow.getCell(13).setCellValue(prezenta.getMat().getNume());
                } else {
                    currentRow.createCell(13, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(13).setCellValue(ifnull(nota_dc.getMat()));
                }

                if (prezenta.getFinal1().getId() > 1) {
                    currentRow.getCell(14).setCellValue(prezenta.getFinal1().getNume());
                } else {
                    currentRow.createCell(14, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(14).setCellValue(ifnull(nota_dc.getMedia()));
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getRaportPrezentaRom() {
        getRaportPrezenta(1, 0, "LIMBA ROMÂNĂ", "23");
    }

    public void getRaportPrezentaMag() {
        getRaportPrezenta(2, 0, "LIMBA MATERNĂ", "24");
    }

    public void getRaportPrezentaMat() {
        getRaportPrezenta(3, 0, "MATEMATICĂ", "25");
    }

    public void getRaportCCPrezentaRom() {
        getRaportPrezenta(1, 1, "LIMBA ROMÂNĂ", "23");
    }

    public void getRaportCCPrezentaMag() {
        getRaportPrezenta(2, 1, "LIMBA MATERNĂ", "24");
    }

    public void getRaportCCPrezentaMat() {
        getRaportPrezenta(3, 1, "MATEMATICĂ", "25");
    }

    public void getRaportPrezenta(int reportType, int grouping, String disciplina, String data) {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "raportPrezenta_" + reportType + ".xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/raport_prezenta.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);

            Object[] results;

            if (userBean.isISJ()) {
                results = elevFacade.getRaportPrezenta(reportType, 2, userBean.getSirues());

            } else {
                results = elevFacade.getRaportPrezenta(reportType, grouping, userBean.getSirues());
            }

            try {


                if (grouping == 1) {
                    mainSheet.getRow(0).getCell(0).setCellValue("CENTRU COMUNICARE:");
                }

                mainSheet.getRow(0).getCell(2).setCellValue(userBean.getNumeUnitate());
                mainSheet.getRow(4).getCell(4).setCellValue(data);
                mainSheet.getRow(5).getCell(4).setCellValue(disciplina);

                Row row = mainSheet.getRow(9);

                row.getCell(0).setCellValue((long) results[0]);
                row.getCell(1).setCellValue(((BigDecimal) results[2]).longValue());
                row.getCell(2).setCellValue(((BigDecimal) results[3]).longValue());
                row.getCell(3).setCellValue(((BigDecimal) results[1]).longValue());
                row.getCell(4).setCellValue(((BigDecimal) results[5]).longValue());
                row.getCell(5).setCellValue(((BigDecimal) results[4]).longValue());
                row.getCell(6).setCellValue(((BigDecimal) results[1]).longValue());
                row.getCell(7).setCellValue(((BigDecimal) results[6]).longValue());
                row.getCell(8).setCellValue(((BigDecimal) results[7]).longValue());
            } catch (NullPointerException nex) {
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getCatalogNotare(int sirues) {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 5;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "catalog_notare.xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/catalog_notare.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);
            DataFormat format = template.createDataFormat();
            style = template.createCellStyle();
            style.setDataFormat(format.getFormat("#0.00"));
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");


            mainSheet.getHeader().setLeft(mainSheet.getHeader().getLeft() + " " + userBean.getNumeUnitate());

            List<Elev> elevList;

            if (sirues == 0) {
                elevList = elevFacade.findByCZE(userBean.getSirues());
            } else {
                elevList = elevFacade.findByPJ(sirues);
            }

            for (int i = 0; i < elevList.size(); i++) {
                Elev elev = elevList.get(i);
                Prezenta prezenta = elev.getPrezentaId();
                NotaIc nota_ic = elev.getNotaIcId();

                Row currentRow = mainSheet.getRow(i + headerOffset);
                if (currentRow == null) {
                    currentRow = mainSheet.createRow(i + headerOffset);
                }
                for (int j = 0; j < 13; j++) {
                    Cell newCell = currentRow.createCell(j);
                }
                //nr. Crt
                currentRow.createCell(0, Cell.CELL_TYPE_NUMERIC);
                currentRow.getCell(0).setCellValue(i + 1);
                //Structura arondată
                currentRow.getCell(1).setCellValue(elev.getUnitate().getNume());
                //Secția
                currentRow.getCell(2).setCellValue(elev.getSectiaId().getNume());
                //Mediul
                currentRow.getCell(3).setCellValue(elev.getMediuId().getNume());
                //Nume
                currentRow.getCell(4).setCellValue(elev.getNume());
                //Ini
                currentRow.getCell(5).setCellValue(elev.getIni());
                //Prenume
                currentRow.getCell(6).setCellValue(elev.getPrenume());
                //CNP
                currentRow.createCell(7, Cell.CELL_TYPE_NUMERIC);
                currentRow.getCell(7).setCellValue(elev.getCnp());
                //Seria
                currentRow.getCell(8).setCellValue(elev.getSeriaId().getNume());

                //ROM
                if (prezenta.getRom().getId() > 1) {
                    currentRow.getCell(9).setCellValue(prezenta.getRom().getNume());
                } else {
                    currentRow.createCell(9, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(9).setCellValue(ifnull(nota_ic.getRom()));
                }
                //MAG
                if (elev.getSectiaId().getId() == 1) {

                    if (prezenta.getMag().getId() > 1) {
                        currentRow.getCell(10).setCellValue(prezenta.getMag().getNume());
                    } else {
                        currentRow.createCell(10, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                        currentRow.getCell(10).setCellValue(ifnull(nota_ic.getMag()));
                    }
                } else {
                    currentRow.getCell(10).setCellValue("-");
                }
                //MAT
                if (prezenta.getMat().getId() > 1) {
                    currentRow.getCell(11).setCellValue(prezenta.getMat().getNume());
                } else {
                    currentRow.createCell(11, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(11).setCellValue(ifnull(nota_ic.getMat()));
                }

                //FINAL
                if (prezenta.getFinal1().getId() > 1) {
                    currentRow.getCell(12).setCellValue(prezenta.getFinal1().getNume());
                } else {
                    //Nota FINAL IC
                    currentRow.createCell(12, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                    currentRow.getCell(12).setCellValue(ifnull(nota_ic.getMedia()));
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getRaportNoteRom() {
        getRaportNote(1, "ROMÂNĂ");
    }

    public void getRaportNoteMag() {
        getRaportNote(2, "MATERNĂ");
    }

    public void getRaportNoteMat() {
        getRaportNote(3, "MATEMATICĂ");
    }

    public void getRaportNoteMedia() {
        getRaportNote(4, "MEDIA");
    }

    public void getRaportNote(int disciplinaType, String headerText) {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 2;
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "raport_note_" + disciplinaType + ".xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/raport_note.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);

            style = template.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);


            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");

            int groupType;

            if (userBean.isISJ()) {
                groupType = 2;
            } else {
                groupType = 1;
            }

            mainSheet.getRow(0).getCell(0).setCellValue(headerText);

            List<Object[]> elevList = elevFacade.getRaportRezultate(disciplinaType, groupType, userBean.getSirues());

            for (int i = 0; i < elevList.size(); i++) {
                Object[] data = elevList.get(i);
                Row currentRow = mainSheet.createRow(i + headerOffset);
                currentRow.createCell(0, Cell.CELL_TYPE_STRING).setCellStyle(style);
                for (int j = 1; j < data.length; j++) {
                    currentRow.createCell(j, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                }
                currentRow.getCell(0).setCellValue((String) data[0]);
                currentRow.getCell(1).setCellValue((Long) data[1]);

                for (int j = 2; j < data.length; j++) {
                    currentRow.getCell(j).setCellValue(((BigDecimal) data[j]).longValue());
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getRaportCNEERom() {
        getRaportCNEE(1, "ROMÂNĂ", false);
    }

    public void getRaportCNEEMag() {
        getRaportCNEE(2, "MATERNĂ", false);
    }

    public void getRaportCNEEMat() {
        getRaportCNEE(3, "MATEMATICĂ", false);
    }

    public void getRaportCNEEMedia() {
        getRaportCNEE(4, "MEDIA", false);
    }

    public void getRaportCNEERomDc() {
        getRaportCNEE(1, "ROMÂNĂ", true);
    }

    public void getRaportCNEEMagDc() {
        getRaportCNEE(2, "MATERNĂ", true);
    }

    public void getRaportCNEEMatDc() {
        getRaportCNEE(3, "MATEMATICĂ", true);
    }

    public void getRaportCNEEMediaDc() {
        getRaportCNEE(4, "MEDIA", true);
    }

    public void getRaportCNEE(int disciplinaType, String headerText, boolean dupa_contestatii) {
        InputStream templateInputStream;
        OutputStream output;
        Workbook template;
        Sheet mainSheet;
        CellStyle style;
        int headerOffset = 2;
        String isFinal = "";
        if (dupa_contestatii) {
            isFinal = "final_";
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
        ec.setResponseContentType("application/vnd.ms-excel"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "raport_cnee_" + isFinal + disciplinaType + ".xls" + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
        try {
            templateInputStream = ec.getResourceAsStream("/resources/templates/raport_cnee.xls");
            template = WorkbookFactory.create(templateInputStream);
            mainSheet = template.getSheetAt(0);

            style = template.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);


            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");


            mainSheet.getRow(0).createCell(0).setCellValue(headerText);

            List<Object[]> elevListTotal = elevFacade.getRaportCNEE(disciplinaType, 0, dupa_contestatii);
            List<Object[]> elevListMediu = elevFacade.getRaportCNEE(disciplinaType, 1, dupa_contestatii);
            Collections.reverse(elevListMediu);
            List<Object[]> elevListSex = elevFacade.getRaportCNEE(disciplinaType, 2, dupa_contestatii);

            List<Object[]> elevList = new ArrayList<>();
            elevList.addAll(elevListTotal);
            elevList.addAll(elevListMediu);
            elevList.addAll(elevListSex);

            for (int i = 0; i < elevList.size(); i++) {
                Object[] data = elevList.get(i);
                Row currentRow = mainSheet.getRow(i + headerOffset);
                /*currentRow.createCell(1, Cell.CELL_TYPE_STRING).setCellStyle(style);
                 for (int j = 2; j < data.length + 1; j++) {
                 currentRow.createCell(j, Cell.CELL_TYPE_NUMERIC).setCellStyle(style);
                 }*/
                //currentRow.getCell(1).setCellValue((String) data[0]);
                currentRow.getCell(2).setCellValue((Long) data[1]);

                for (int j = 3; j < data.length + 1; j++) {
                    currentRow.getCell(j).setCellValue(((BigDecimal) data[j - 1]).longValue());
                }
            }
            templateInputStream.close();
            output = ec.getResponseOutputStream();
            template.write(output);
            output.flush();
            output.close();
            fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(ExportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
