package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import ro.deimios.dcevnat2013.facade.MediuFacadeLocal;
import ro.deimios.dcevnat2013.facade.SectiaFacadeLocal;
import ro.deimios.dcevnat2013.facade.SeriaFacadeLocal;
import ro.deimios.dcevnat2013.facade.StareFacadeLocal;
import ro.deimios.dcevnat2013.facade.StarePrezentaFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ApplicationScoped
public class NomenclatorListerBean implements Serializable {

    @EJB
    private StarePrezentaFacadeLocal starePrezentaFacade;
    @EJB
    private StareFacadeLocal stareFacade;
    @EJB
    private MediuFacadeLocal mediuFacade;
    @EJB
    private SectiaFacadeLocal sectiaFacade;
    @EJB
    private SeriaFacadeLocal seriaFacade;
    private List seriaList;
    private List sectiaList;
    private List mediuList;
    private List stareList;
    private List starePrezentaList;
    private List<SelectItem> daNuList;

    @PostConstruct
    public void loadData() {
        seriaList = seriaFacade.findAll();
        sectiaList = sectiaFacade.findAll();
        mediuList = mediuFacade.findAll();
        stareList = stareFacade.findAll();
        starePrezentaList = starePrezentaFacade.findAll();
    }

    /**
     * Creates a new instance of SeriaListerBean
     */
    public NomenclatorListerBean() {
        daNuList = new ArrayList<>(2);
        daNuList.add(new SelectItem("", "Toți"));
        daNuList.add(new SelectItem(1, "Da"));
        daNuList.add(new SelectItem(0, "Nu"));
    }

    public List getSeriaList() {
        return seriaList;
    }

    public List getSectiaList() {
        return sectiaList;
    }

    public List getMediuList() {
        return mediuList;
    }

    public List getStareList() {
        return stareList;
    }

    public List<SelectItem> getDaNuList() {
        return daNuList;
    }

    public List getStarePrezentaList() {
        return starePrezentaList;
    }
}
