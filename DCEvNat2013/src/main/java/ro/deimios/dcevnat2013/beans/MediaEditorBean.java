package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.facade.Media58FacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class MediaEditorBean implements Serializable {

    @EJB
    private Media58FacadeLocal media58Facade;
    
    /**
     * Creates a new instance of MediaEditorBean
     */
    public MediaEditorBean() {
    }
    
    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Elev elev = (Elev) source.getRowData();
        media58Facade.edit(elev.getMedia58Id());
    }
}
