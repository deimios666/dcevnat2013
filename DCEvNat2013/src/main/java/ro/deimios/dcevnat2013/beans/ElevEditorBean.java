/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ElevEditorBean implements Serializable {

    @EJB
    private ElevFacadeLocal elevFacade;

    /**
     * Creates a new instance of ElevEditorBean
     */
    public ElevEditorBean() {
    }

    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Elev elev = (Elev) source.getRowData();
        elevFacade.edit(elev);
    }
}
