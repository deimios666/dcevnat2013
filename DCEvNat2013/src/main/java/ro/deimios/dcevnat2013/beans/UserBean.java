package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.entity.Users;
import ro.deimios.dcevnat2013.facade.UsersFacadeLocal;
import ro.deimios.dcevnat2013.helpers.CryptHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

    public static int SIRUES_ISJ = 458659;
    @EJB
    private UsersFacadeLocal usersFacade;
    private String userName;
    private String password;
    private String newpassword1;
    private String newpassword2;
    private Integer sirues = 0;
    private String numeUnitate;
    private boolean authenticated = false;
    private boolean ISJ = false;
    private boolean CC = false;
    private boolean CZE = false;
    private Users user;

    /**
     * Creates a new instance of UserBean
     */
    public UserBean() {
    }

    public void doLogin() {
        user = usersFacade.authenticate(userName, password);
        if (user != null) {
            Unitate unitate = user.getUnitate();
            sirues = unitate.getSirues();
            numeUnitate = unitate.getNume();
            ISJ = (unitate.getSirues() == SIRUES_ISJ);
            CC = unitate.getIsCentruComunicare();
            CZE = unitate.getIsCentruEvaluare();
            authenticated = true;
        } else {
            FacesMessage msg = new FacesMessage("Eroare autentificare", "Nume utilizator sau parolă greșită");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void doChangePassword() {
        if (authenticated && (newpassword1.equals(newpassword2))) {
            user.setPassword(CryptHelper.sha512(newpassword1));
            usersFacade.edit(user);
            FacesMessage msg = new FacesMessage("Salvat", "Parola a fost schimbată");
            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            FacesMessage msg = new FacesMessage("Eroare schimbare parolă", "Parolele nu sunt identice");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void doLogout() {
        userName = null;
        password = null;
        authenticated = false;
        numeUnitate = null;
        sirues = 0;
        ISJ = false;
        CC = false;
        CZE = false;
        user = null;
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = CryptHelper.sha512(password);
    }

    public Integer getSirues() {
        return sirues;
    }

    public String getNumeUnitate() {
        return numeUnitate;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public boolean isISJ() {
        return ISJ;
    }

    public boolean isCC() {
        return CC;
    }

    public boolean isCZE() {
        return CZE;
    }

    public String getNewpassword1() {
        return newpassword1;
    }

    public void setNewpassword1(String newpassword1) {
        this.newpassword1 = newpassword1;
    }

    public String getNewpassword2() {
        return newpassword2;
    }

    public void setNewpassword2(String newpassword2) {
        this.newpassword2 = newpassword2;
    }

}
