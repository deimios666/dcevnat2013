package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dcevnat2013.datamodel.LazyElevModel;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ElevListerBean implements Serializable {

    private LazyDataModel<Elev> elevList;

    /**
     * Creates a new instance of ElevListerBean
     */
    public ElevListerBean() {
    }

    @PostConstruct
    public void loadData() {
        FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        elevList = new LazyElevModel(((UserBean) BeanHelper.getBean("userBean")).getSirues());
    }

    public LazyDataModel<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(LazyDataModel<Elev> elevList) {
        this.elevList = elevList;
    }
}
