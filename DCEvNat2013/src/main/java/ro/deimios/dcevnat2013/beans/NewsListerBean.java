/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.beans;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ro.deimios.dcevnat2013.entity.Notifications;
import ro.deimios.dcevnat2013.facade.NotificationsFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class NewsListerBean {

    @EJB
    private NotificationsFacadeLocal notificationsFacade;

    /**
     * Creates a new instance of NewsListerBean
     */
    public NewsListerBean() {
    }

    public List<Notifications> getNews() {
        int sirues = ((UserBean) BeanHelper.getBean("userBean")).getSirues();
        return notificationsFacade.findBySirues(sirues);
    }
}
