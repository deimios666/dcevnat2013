package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.entity.Prezenta;
import ro.deimios.dcevnat2013.facade.PrezentaFacadeLocal;
import ro.deimios.dcevnat2013.facade.StarePrezentaFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class PrezentaEditorBean implements Serializable {

    @EJB
    private StarePrezentaFacadeLocal starePrezentaFacade;
    @EJB
    private PrezentaFacadeLocal prezentaFacade;

    /**
     * Creates a new instance of PrezentaEditorBean
     */
    public PrezentaEditorBean() {
    }

    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Elev elev = (Elev) source.getRowData();
        Prezenta prezenta = elev.getPrezentaId();
        //if neprezentat, final should not be prezent
        if ((prezenta.getRom().getId() == 2 || prezenta.getMag().getId() == 2 || prezenta.getMat().getId() == 2) && prezenta.getFinal1().getId() == 1) {
            prezenta.setFinal1(starePrezentaFacade.find(2));
        }
        //if eliminat, final is eliminat
        if ((prezenta.getRom().getId() == 3 || prezenta.getMag().getId() == 3 || prezenta.getMat().getId() == 3) && prezenta.getFinal1().getId() != 3) {
            prezenta.setFinal1(starePrezentaFacade.find(3));
        }
        prezentaFacade.edit(elev.getPrezentaId());
    }
}
