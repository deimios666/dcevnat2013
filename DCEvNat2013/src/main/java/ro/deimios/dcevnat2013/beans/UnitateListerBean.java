/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dcevnat2013.datamodel.LazyUnitateModel;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.facade.UnitateFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class UnitateListerBean implements Serializable {

    @EJB
    private UnitateFacadeLocal unitateFacade;
    private LazyDataModel<Unitate> unitateList;

    /**
     * Creates a new instance of UnitateListerBean
     */
    public UnitateListerBean() {
    }

    @PostConstruct
    public void loadData() {
        unitateList = new LazyUnitateModel();
    }

    public List<Unitate> complete(String query) {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.isISJ()) {
            return unitateFacade.searchByNumeOrdered(query);
        } else {
            return unitateFacade.searchBySiruesSuperiorNume(userBean.getSirues(), query);
        }
    }

    public List<Unitate> getARList() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.isISJ()) {
            return unitateFacade.findAllOrdered();
        } else {
            return unitateFacade.findBySiruesSuperior(userBean.getSirues());
        }
    }

    public List<Unitate> getPJList() {
        return unitateFacade.findAllPJ();
    }

    public List<Unitate> getUnitateByCZE() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.isISJ()) {
            return unitateFacade.findAllPJ();
        } else {
            return unitateFacade.findByCZE(userBean.getSirues());
        }
    }

    public LazyDataModel<Unitate> getUnitateList() {
        return unitateList;
    }

    public void setUnitateList(LazyDataModel<Unitate> unitateList) {
        this.unitateList = unitateList;
    }

}
