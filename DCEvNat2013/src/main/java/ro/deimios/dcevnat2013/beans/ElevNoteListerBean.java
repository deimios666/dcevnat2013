package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import ro.deimios.dcevnat2013.datamodel.LazyElevNoteModel;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ElevNoteListerBean implements Serializable {

    private LazyDataModel<Elev> elevList;

    /**
     * Creates a new instance of ElevListerBean
     */
    public ElevNoteListerBean() {
    }

    @PostConstruct
    public void loadData() {
        FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        int listType = 0;
        if (userBean.isISJ()) {
            listType = 2;
        }
        if (userBean.isCZE()) {
            listType = 1;
        }
        elevList = new LazyElevNoteModel(((UserBean) BeanHelper.getBean("userBean")).getSirues(), listType);
    }

    public LazyDataModel<Elev> getElevList() {
        return elevList;
    }

    public void setElevList(LazyDataModel<Elev> elevList) {
        this.elevList = elevList;
    }
}
