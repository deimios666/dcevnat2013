package ro.deimios.dcevnat2013.beans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Users;
import ro.deimios.dcevnat2013.facade.UsersFacadeLocal;
import ro.deimios.dcevnat2013.helpers.CryptHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class UsersEditorBean {

    UsersFacadeLocal usersFacade = lookupUsersFacadeLocal();

    /**
     * Creates a new instance of UsersEditorBean
     */
    public UsersEditorBean() {
    }

    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Users users = (Users) source.getRowData();
        //ugly - hash only if not hashed already, breaks if password is 128 or longer
        if (users.getPassword().length() < 128) {
            users.setPassword(CryptHelper.sha512(users.getPassword()));
        }
        usersFacade.edit(users);
    }

    private UsersFacadeLocal lookupUsersFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (UsersFacadeLocal) c.lookup("java:global/DCEvNat/UsersFacade!ro.deimios.dcevnat2013.facade.UsersFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
