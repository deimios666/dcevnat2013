package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.entity.Media58;
import ro.deimios.dcevnat2013.entity.Mediu;
import ro.deimios.dcevnat2013.entity.Nota;
import ro.deimios.dcevnat2013.entity.NotaDc;
import ro.deimios.dcevnat2013.entity.NotaIc;
import ro.deimios.dcevnat2013.entity.Notifications;
import ro.deimios.dcevnat2013.entity.Prezenta;
import ro.deimios.dcevnat2013.entity.Sectia;
import ro.deimios.dcevnat2013.entity.Seria;
import ro.deimios.dcevnat2013.entity.StarePrezenta;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;
import ro.deimios.dcevnat2013.facade.Media58FacadeLocal;
import ro.deimios.dcevnat2013.facade.MediuFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaDcFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaIcFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotificationsFacadeLocal;
import ro.deimios.dcevnat2013.facade.PrezentaFacadeLocal;
import ro.deimios.dcevnat2013.facade.SectiaFacadeLocal;
import ro.deimios.dcevnat2013.facade.SeriaFacadeLocal;
import ro.deimios.dcevnat2013.facade.StareFacadeLocal;
import ro.deimios.dcevnat2013.facade.StarePrezentaFacadeLocal;
import ro.deimios.dcevnat2013.facade.UnitateFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class ElevAddBean implements Serializable {

    @EJB
    private StarePrezentaFacadeLocal starePrezentaFacade;
    @EJB
    private PrezentaFacadeLocal prezentaFacade;
    @EJB
    private NotificationsFacadeLocal notificationsFacade;
    @EJB
    private MediuFacadeLocal mediuFacade;
    @EJB
    private SectiaFacadeLocal sectiaFacade;
    @EJB
    private SeriaFacadeLocal seriaFacade;
    @EJB
    private UnitateFacadeLocal unitateFacade;
    @EJB
    private StareFacadeLocal stareFacade;
    @EJB
    private NotaFacadeLocal notaFacade;
    @EJB
    private NotaIcFacadeLocal notaIcFacade;
    @EJB
    private NotaDcFacadeLocal notaDcFacade;
    @EJB
    private Media58FacadeLocal media58Facade;
    @EJB
    private ElevFacadeLocal elevFacade;
    String cnp;
    boolean editingMode = false;
    boolean askForTransfer = false;
    Elev elev;
    String nume;
    String ini;
    String prenume;
    Unitate unitate;
    Seria seria;
    Mediu mediu;
    Sectia sectia;

    /**
     * Creates a new instance of ElevAddBean
     */
    public ElevAddBean() {
    }

    public void searchElev() {
        Long searchCNP = Long.valueOf(cnp);
        elev = elevFacade.find(searchCNP);
        if (elev == null) {
            //enable editing
            editingMode = true;
        } else {
            //ask for transfer
            askForTransfer = true;

        }
    }

    public void doTransfer() {

        String deLa = elev.getUnitate().getNume();
        Unitate oldUnitate=elev.getUnitate();
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        Unitate newUnitate = unitateFacade.find(userBean.getSirues());
        String la = newUnitate.getNume();
        elev.setUnitate(newUnitate);
        elevFacade.edit(elev);
        String mesaj = "Elevul " + elev.getNume() + " " + elev.getIni() + " "
                + elev.getPrenume() + " a fost transferat de la " + deLa + " la " + la;
        Notifications messg = new Notifications(1, mesaj, new Date(), true);
        messg.setUnitateSirues(oldUnitate);
        notificationsFacade.create(messg);
        FacesMessage msg = new FacesMessage("Elev Transferat", mesaj);
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        askForTransfer = false;
        elev = null;
    }

    public void saveElev() {

        Elev newElev = new Elev(Long.valueOf(cnp), nume, ini, prenume);
        //-------
        Nota nota = new Nota(1);
        nota.setRom(BigDecimal.ZERO);
        nota.setMag(BigDecimal.ZERO);
        nota.setMat(BigDecimal.ZERO);
        nota.setMedia(BigDecimal.ZERO);
        notaFacade.create(nota);
        //-------
        NotaIc notaIc = new NotaIc(1);
        notaIc.setRom(BigDecimal.ZERO);
        notaIc.setMag(BigDecimal.ZERO);
        notaIc.setMat(BigDecimal.ZERO);
        notaIc.setMedia(BigDecimal.ZERO);
        notaIcFacade.create(notaIc);
        //-------
        NotaDc notaDc = new NotaDc(1);
        notaDc.setRom(BigDecimal.ZERO);
        notaDc.setMag(BigDecimal.ZERO);
        notaDc.setMat(BigDecimal.ZERO);
        notaDc.setMedia(BigDecimal.ZERO);
        notaDcFacade.create(notaDc); 
        //-------
        Media58 media58 = new Media58(1);
        media58.setInscris(false);
        media58.setStareId(stareFacade.find(1));
        media58.setMedia(BigDecimal.ZERO);
        media58Facade.create(media58);
        //-------
        StarePrezenta starePrezenta = starePrezentaFacade.find(1);
        Prezenta prezenta = new Prezenta(1);
        prezenta.setRom(starePrezenta);
        prezenta.setMag(starePrezenta);
        prezenta.setMat(starePrezenta);
        prezenta.setFinal1(starePrezenta);
        prezentaFacade.create(prezenta);
        //-------
        newElev.setNotaId(nota);
        newElev.setNotaIcId(notaIc);
        newElev.setNotaDcId(notaDc);
        newElev.setMedia58Id(media58);
        newElev.setUnitate(unitate);
        newElev.setMediuId(mediu);
        newElev.setSectiaId(sectia);
        newElev.setSeriaId(seria);
        newElev.setPrezentaId(prezenta);
        elevFacade.create(newElev);
        editingMode = false;

        FacesMessage msg = new FacesMessage("Elev Salvat", "Elevul " + nume + " " + ini + " " + prenume + " a fost salvat cu succes!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public boolean isEditingMode() {
        return editingMode;
    }

    public boolean isAskForTransfer() {
        return askForTransfer;
    }

    public Elev getElev() {
        return elev;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getIni() {
        return ini;
    }

    public void setIni(String ini) {
        this.ini = ini;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public Unitate getUnitate() {
        return unitate;
    }

    public void setUnitate(Unitate unitate) {
        this.unitate = unitateFacade.find(unitate.getSirues());
    }

    public Seria getSeria() {
        return seria;
    }

    public void setSeria(Seria seria) {
        this.seria = seriaFacade.find(seria.getId());
    }

    public Mediu getMediu() {
        return mediu;
    }

    public void setMediu(Mediu mediu) {
        this.mediu = mediuFacade.find(mediu.getId());
    }

    public void setSectia(Sectia sectia) {
        this.sectia = sectiaFacade.find(sectia.getId());
    }

    public Sectia getSectia() {
        return sectia;
    }
}
