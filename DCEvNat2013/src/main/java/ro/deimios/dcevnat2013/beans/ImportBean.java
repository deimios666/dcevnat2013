package ro.deimios.dcevnat2013.beans;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;
import ro.deimios.dcevnat2013.entity.Elev;
import ro.deimios.dcevnat2013.entity.Media58;
import ro.deimios.dcevnat2013.entity.Nota;
import ro.deimios.dcevnat2013.entity.NotaDc;
import ro.deimios.dcevnat2013.entity.NotaIc;
import ro.deimios.dcevnat2013.entity.Stare;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;
import ro.deimios.dcevnat2013.facade.Media58FacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaDcFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaFacadeLocal;
import ro.deimios.dcevnat2013.facade.NotaIcFacadeLocal;
import ro.deimios.dcevnat2013.facade.StareFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;
import ro.deimios.dcevnat2013.helpers.NotaHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@RequestScoped
public class ImportBean {

    @EJB
    private NotaFacadeLocal notaFacade;
    @EJB
    private NotaDcFacadeLocal notaDcFacade;
    @EJB
    private NotaIcFacadeLocal notaIcFacade;
    @EJB
    private Media58FacadeLocal media58Facade;
    @EJB
    private StareFacadeLocal stareFacade;
    @EJB
    private ElevFacadeLocal elevFacade;
    private Map<String, Integer> stareMap;
    public static final Map<String, Boolean> danu = new HashMap<>();

    static {
        danu.put("Da", Boolean.TRUE);
        danu.put("Nu", Boolean.FALSE);
    }

    /**
     * Creates a new instance of ImportBean
     */
    public ImportBean() {
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            //open uplaoded file iostream
            InputStream fis = event.getFile().getInputstream();
            //create workbook from stream
            Workbook workbook = WorkbookFactory.create(fis);
            //get first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();

            int readData = 0;
            int cnpError = 0;
            int unitateError = 0;
            int parseError = 0;
            List<Stare> stareList = stareFacade.findAll();
            stareMap = new HashMap<>();
            for (int i = 0; i < stareList.size(); i++) {
                stareMap.put(stareList.get(i).getNume(), stareList.get(i).getId());
            }

            DecimalFormat df = new DecimalFormat("#0.00");

            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    Long cnp = Math.round(row.getCell(0).getNumericCellValue());
                    Elev elev = elevFacade.find(cnp);
                    if (elev != null) {
                        int sir1 = elev.getUnitate().getUnitateSuperioara().getSirues();
                        int sir2 = userBean.getSirues();

                        if (sir1 == sir2) {
                            Media58 media58 = elev.getMedia58Id();

                            boolean inscris = danu.get(row.getCell(1).getStringCellValue());

                            Stare stare = stareFacade.find(stareMap.get(row.getCell(3).getStringCellValue()));
                            String sMedia = df.format(row.getCell(2).getNumericCellValue());
                            BigDecimal media = new BigDecimal(sMedia);
                            media58.setInscris(inscris);
                            media58.setMedia(media);
                            media58.setStareId(stare);

                            media58Facade.edit(media58);

                            elev.setMedia58Id(media58);

                            elevFacade.edit(elev);

                            readData++;
                        } else {
                            unitateError++;

                        }
                    } else {
                        cnpError++;
                    }
                } catch (Exception parseEx) {
                    parseError++;
                    System.out.println(parseEx);
                }
            }

            //close streams, delete file
            fis.close();

            //give feedback

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " rânduri au fost importate cu succes! " + cnpError + " cnp-uri negăsite, " + unitateError + " elevi la alte unități, " + parseError + " erori de citire/format!");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (InvalidFormatException ex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul are format invalid!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ioex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul nu a fost uploadat!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void handleNoteIcFileUpload(FileUploadEvent event) {
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            //open uplaoded file iostream
            InputStream fis = event.getFile().getInputstream();
            //create workbook from stream
            Workbook workbook = WorkbookFactory.create(fis);
            //get first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();

            int readData = 0;
            int cnpError = 0;
            int unitateError = 0;
            int parseError = 0;

            DecimalFormat df = new DecimalFormat("#0.00");
            int sir2 = userBean.getSirues();
            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    Long cnp = Math.round(row.getCell(4).getNumericCellValue());
                    Elev elev = elevFacade.find(cnp);
                    if (elev != null) {
                        int sir1 = elev.getUnitate().getUnitateSuperioara().getCentruEvaluare().getSirues();
                        if (sir1 == sir2) {
                            NotaIc nota_ic = elev.getNotaIcId();
                            NotaDc nota_dc = elev.getNotaDcId();
                            Nota nota = elev.getNotaId();

                            try {
                                String sRom = df.format(row.getCell(11).getNumericCellValue());
                                BigDecimal rom = new BigDecimal(sRom);
                                nota_ic.setRom(rom);
                                nota_dc.setRom(rom);
                                nota.setRom(rom);

                            } catch (Exception ex) {
                            }

                            try {
                                String sMag = df.format(row.getCell(12).getNumericCellValue());
                                BigDecimal mag = new BigDecimal(sMag);
                                nota_ic.setMag(mag);
                                nota_dc.setMag(mag);
                                nota.setMag(mag);
                            } catch (Exception ex) {
                            }

                            try {
                                String sMat = df.format(row.getCell(13).getNumericCellValue());
                                BigDecimal mat = new BigDecimal(sMat);
                                nota_ic.setMat(mat);
                                nota_dc.setMat(mat);
                                nota.setMat(mat);
                            } catch (Exception ex) {
                            }

                            if (elev.getSectiaId().getId() == 1) {
                                nota_ic.setMedia(NotaHelper.average(nota_ic.getRom(), nota_ic.getMag(), nota_ic.getMat()));
                                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMag(), nota_dc.getMat()));
                                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMag(), nota.getMat()));
                            } else {
                                nota_ic.setMedia(NotaHelper.average(nota_ic.getRom(), nota_ic.getMat()));
                                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMat()));
                                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMat()));
                            }

                            notaIcFacade.edit(nota_ic);
                            notaDcFacade.edit(nota_dc);
                            notaFacade.edit(nota);

                            //elev.setNotaIcId(nota_ic);
                            //elevFacade.edit(elev);

                            readData++;
                        } else {
                            unitateError++;
                        }
                    } else {
                        cnpError++;
                    }
                } catch (Exception parseEx) {
                    parseError++;
                    System.out.println(parseEx);
                }
            }

            //close streams, delete file
            fis.close();

            //give feedback

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " rânduri au fost importate cu succes! " + cnpError + " cnp-uri negăsite, " + unitateError + " elevi la alte unități, " + parseError + " erori de citire/format!");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (InvalidFormatException ex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul are format invalid!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ioex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul nu a fost uploadat!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void handleNoteDcFileUpload(FileUploadEvent event) {
        try {
            UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
            //open uplaoded file iostream
            InputStream fis = event.getFile().getInputstream();
            //create workbook from stream
            Workbook workbook = WorkbookFactory.create(fis);
            //get first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowWalker = sheet.rowIterator();
            Row ignoredHeaderRow = rowWalker.next();

            int readData = 0;
            int cnpError = 0;
            int unitateError = 0;
            int parseError = 0;

            DecimalFormat df = new DecimalFormat("#0.00");
            int sir2 = userBean.getSirues();
            while (rowWalker.hasNext()) {
                Row row = rowWalker.next();
                try {
                    Long cnp = Math.round(row.getCell(4).getNumericCellValue());
                    Elev elev = elevFacade.find(cnp);
                    if (elev != null) {
                        int sir1 = elev.getUnitate().getUnitateSuperioara().getCentruEvaluare().getSirues();
                        if (sir1 == sir2) {
                            NotaIc nota_ic = elev.getNotaIcId();
                            NotaDc nota_dc = elev.getNotaDcId();
                            Nota nota = elev.getNotaId();

                            try {
                                String sRom = df.format(row.getCell(11).getNumericCellValue());
                                BigDecimal rom = new BigDecimal(sRom);
                                //nota_ic.setRom(rom);
                                nota_dc.setRom(rom);
                                nota.setRom(NotaHelper.contestat(nota_ic.getRom(), rom));

                            } catch (Exception ex) {
                            }

                            try {
                                String sMag = df.format(row.getCell(12).getNumericCellValue());
                                BigDecimal mag = new BigDecimal(sMag);
                                nota_dc.setMag(mag);
                                nota.setMag(NotaHelper.contestat(nota_ic.getMag(), mag));
                            } catch (Exception ex) {
                            }

                            try {
                                String sMat = df.format(row.getCell(13).getNumericCellValue());
                                BigDecimal mat = new BigDecimal(sMat);
                                nota_dc.setMat(mat);
                                nota.setMat(NotaHelper.contestat(nota_ic.getMat(), mat));
                            } catch (Exception ex) {
                            }

                            if (elev.getSectiaId().getId() == 1) {
                                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMag(), nota_dc.getMat()));
                                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMag(), nota.getMat()));
                            } else {
                                nota_dc.setMedia(NotaHelper.average(nota_dc.getRom(), nota_dc.getMat()));
                                nota.setMedia(NotaHelper.average(nota.getRom(), nota.getMat()));
                            }

                            notaDcFacade.edit(nota_dc);
                            notaFacade.edit(nota);

                            //elev.setNotaIcId(nota_ic);
                            //elevFacade.edit(elev);

                            readData++;
                        } else {
                            unitateError++;
                        }
                    } else {
                        cnpError++;
                    }
                } catch (Exception parseEx) {
                    parseError++;
                    System.out.println(parseEx);
                }
            }

            //close streams, delete file
            fis.close();

            //give feedback

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Importat cu Succes", readData + " rânduri au fost importate cu succes! " + cnpError + " cnp-uri negăsite, " + unitateError + " elevi la alte unități, " + parseError + " erori de citire/format!");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (InvalidFormatException ex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul are format invalid!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ioex) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Eroare Upload", "Fișierul nu a fost uploadat!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
