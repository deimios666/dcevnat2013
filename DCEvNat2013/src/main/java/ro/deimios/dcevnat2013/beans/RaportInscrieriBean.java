/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import ro.deimios.dcevnat2013.datamodel.RaportInscriereDataModel;
import ro.deimios.dcevnat2013.facade.ElevFacadeLocal;
import ro.deimios.dcevnat2013.helpers.BeanHelper;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class RaportInscrieriBean implements Serializable {

    @EJB
    private ElevFacadeLocal elevFacade;
    List<RaportInscriereDataModel> raport;

    @PostConstruct
    public void loadData() {
        UserBean userBean = (UserBean) BeanHelper.getBean("userBean");
        if (userBean.isISJ()) {
            raport = elevFacade.getRaportInscriere(0);
        } else {
            raport = elevFacade.getRaportInscriere(userBean.getSirues());
        }

    }

    /**
     * Creates a new instance of RaportInscrieriBean
     */
    public RaportInscrieriBean() {
    }

    public List<RaportInscriereDataModel> getRaport() {
        return raport;
    }
}
