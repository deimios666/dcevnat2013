package ro.deimios.dcevnat2013.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.facade.UnitateFacadeLocal;

/**
 *
 * @author deimios
 */
@ManagedBean
@ViewScoped
public class UnitateEditorBean implements Serializable {

    @EJB
    private UnitateFacadeLocal unitateFacade;

    /**
     * Creates a new instance of UnitateEditorBean
     */
    public UnitateEditorBean() {
    }

    public void onCellEdit(CellEditEvent event) {
        DataTable source = (DataTable) event.getSource();
        Unitate unitate = (Unitate) source.getRowData();
        unitateFacade.edit(unitate);
    }

}
