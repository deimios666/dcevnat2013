/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Transferlog;

/**
 *
 * @author deimios
 */
@Local
public interface TransferlogFacadeLocal {

    void create(Transferlog transferlog);

    void edit(Transferlog transferlog);

    void remove(Transferlog transferlog);

    Transferlog find(Object id);

    List<Transferlog> findAll();

    List<Transferlog> findRange(int[] range);

    int count();
    
}
