package ro.deimios.dcevnat2013.facade;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Users;

/**
 *
 * @author deimios
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> implements UsersFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    @Override
    public Users authenticate(String userName, String password) {
        Query query = em.createNamedQuery("Users.findByNamePassword");
        query.setParameter("name", userName);
        query.setParameter("password", password);
        List<Users> userList = query.getResultList();
        if (userList.size() == 1) {
            return userList.get(0);
        } else {
            return null;
        }
    }
    
    @Override
    public List<Users> findUnitateFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select u from Users u";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where u." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and u." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        if (sortField != null) {
            if (sortOrder == SortOrder.ASCENDING) {
                queryString += " order by u." + sortField + " asc";
            }
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " order by u." + sortField + " desc";
            }
            queryString += ", u.name asc";

        } else {
            queryString += " order by u.name asc";
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    @Override
    public long countFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select count(u) as nrUnitati from Users u";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where u." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and u." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }
    
}
