package ro.deimios.dcevnat2013.facade;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Unitate;

/**
 *
 * @author deimios
 */
@Stateless
public class UnitateFacade extends AbstractFacade<Unitate> implements UnitateFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UnitateFacade() {
        super(Unitate.class);
    }

    @Override
    public List<Unitate> findAllOrdered() {
        Query query = em.createNamedQuery("Unitate.findAll");
        return query.getResultList();
    }

    @Override
    public Unitate findByNume(String nume) {
        Query query = em.createNamedQuery("Unitate.findByNume");
        query.setParameter("nume", nume);
        return (Unitate) query.getSingleResult();
    }

    @Override
    public List<Unitate> searchByNumeOrdered(String nume) {
        Query query = em.createNamedQuery("Unitate.searchByNumeOrdered");
        query.setParameter("nume", "%" + nume + "%");
        return query.getResultList();
    }

    @Override
    public List<Unitate> searchBySiruesSuperiorNume(int sirues, String nume) {
        Query query = em.createNamedQuery("Unitate.searchBySiruesSuperiorNume");
        query.setParameter("sirues", sirues);
        query.setParameter("nume", "%" + nume + "%");
        return query.getResultList();
    }

    @Override
    public List<Unitate> findBySiruesSuperior(int sirues) {
        Query query = em.createNamedQuery("Unitate.findBySiruesSuperior");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Unitate> findAllPJ() {
        Query query = em.createNamedQuery("Unitate.findPJ");
        return query.getResultList();
    }

    @Override
    public List<Unitate> findByCZE(int sirues) {
        Query query = em.createNamedQuery("Unitate.findPJByCZE");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Unitate> findUnitateFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select u from Unitate u";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where u." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and u." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        if (sortField != null) {
            if (sortOrder == SortOrder.ASCENDING) {
                queryString += " order by u." + sortField + " asc";
            }
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " order by u." + sortField + " desc";
            }
            queryString += ", u.nume asc";

        } else {
            queryString += " order by u.nume asc";
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    @Override
    public long countFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select count(u) as nrUnitati from Unitate u";

        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();

        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where u." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and u." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

}
