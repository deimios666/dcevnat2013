/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Media58;

/**
 *
 * @author deimios
 */
@Local
public interface Media58FacadeLocal {

    void create(Media58 media58);

    void edit(Media58 media58);

    void remove(Media58 media58);

    Media58 find(Object id);

    List<Media58> findAll();

    List<Media58> findRange(int[] range);

    int count();
    
}
