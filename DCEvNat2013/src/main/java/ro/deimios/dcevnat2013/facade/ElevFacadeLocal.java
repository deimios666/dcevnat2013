/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.datamodel.RaportInscriereDataModel;
import ro.deimios.dcevnat2013.entity.Elev;

/**
 *
 * @author deimios
 */
@Local
public interface ElevFacadeLocal {

    void create(Elev elev);

    void edit(Elev elev);

    void remove(Elev elev);

    Elev find(Object id);

    List<Elev> findAll();

    List<Elev> findRange(int[] range);

    int count();

    public List<RaportInscriereDataModel> getRaportInscriere(int sirues);

    public List<Elev> findElevFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter);

    public long countFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter);

    public List<Elev> findByCZE(int sirues);

    public Object[] getRaportPrezenta(int disciplinaType, int groupType, int sirues);

    public List<Object[]> getRaportRezultate(int disciplinaType, int groupType, int sirues);

    public List<Elev> findByPJ(int sirues);

    public List<Object[]> getRaportCNEE(int disciplinaType, int groupType,boolean dupa_contestatii);
    
}
