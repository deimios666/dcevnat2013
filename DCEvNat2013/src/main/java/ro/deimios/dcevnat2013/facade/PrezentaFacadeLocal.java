/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Prezenta;

/**
 *
 * @author deimios
 */
@Local
public interface PrezentaFacadeLocal {

    void create(Prezenta prezenta);

    void edit(Prezenta prezenta);

    void remove(Prezenta prezenta);

    Prezenta find(Object id);

    List<Prezenta> findAll();

    List<Prezenta> findRange(int[] range);

    int count();
    
}
