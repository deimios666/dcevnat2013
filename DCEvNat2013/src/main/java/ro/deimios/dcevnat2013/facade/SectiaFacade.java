/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.Sectia;

/**
 *
 * @author deimios
 */
@Stateless
public class SectiaFacade extends AbstractFacade<Sectia> implements SectiaFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SectiaFacade() {
        super(Sectia.class);
    }

    @Override
    public Sectia findByNume(String nume) {
        Query query = em.createNamedQuery("Sectia.findByNume");
        query.setParameter("nume", nume);
        return (Sectia) query.getSingleResult();
    }
}
