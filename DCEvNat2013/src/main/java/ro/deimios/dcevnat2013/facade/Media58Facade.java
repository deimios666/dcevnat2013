/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dcevnat2013.entity.Media58;

/**
 *
 * @author deimios
 */
@Stateless
public class Media58Facade extends AbstractFacade<Media58> implements Media58FacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Media58Facade() {
        super(Media58.class);
    }
    
}
