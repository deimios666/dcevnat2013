/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dcevnat2013.entity.Prezenta;

/**
 *
 * @author deimios
 */
@Stateless
public class PrezentaFacade extends AbstractFacade<Prezenta> implements PrezentaFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrezentaFacade() {
        super(Prezenta.class);
    }
    
}
