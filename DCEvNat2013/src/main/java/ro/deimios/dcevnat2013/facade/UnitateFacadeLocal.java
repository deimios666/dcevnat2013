package ro.deimios.dcevnat2013.facade;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.entity.Unitate;

/**
 *
 * @author deimios
 */
@Local
public interface UnitateFacadeLocal {

    void create(Unitate unitate);

    void edit(Unitate unitate);

    void remove(Unitate unitate);

    Unitate find(Object id);

    List<Unitate> findAll();

    List<Unitate> findRange(int[] range);

    int count();

    public Unitate findByNume(String nume);

    public List<Unitate> findBySiruesSuperior(int sirues);

    public List<Unitate> findAllOrdered();

    public List<Unitate> searchBySiruesSuperiorNume(int sirues, String nume);

    public List<Unitate> searchByNumeOrdered(String nume);

    public List<Unitate> findByCZE(int sirues);

    public List<Unitate> findAllPJ();

    public List<Unitate> findUnitateFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter);

    public long countFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter);
}
