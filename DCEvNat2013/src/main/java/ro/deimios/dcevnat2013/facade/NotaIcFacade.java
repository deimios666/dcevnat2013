/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dcevnat2013.entity.NotaIc;

/**
 *
 * @author deimios
 */
@Stateless
public class NotaIcFacade extends AbstractFacade<NotaIc> implements NotaIcFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotaIcFacade() {
        super(NotaIc.class);
    }
    
}
