package ro.deimios.dcevnat2013.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.primefaces.model.SortOrder;
import ro.deimios.dcevnat2013.datamodel.RaportInscriereDataModel;
import ro.deimios.dcevnat2013.entity.Elev;

/**
 *
 * @author deimios
 */
@Stateless
public class ElevFacade extends AbstractFacade<Elev> implements ElevFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ElevFacade() {
        super(Elev.class);
    }

    @Override
    public List<Elev> findByPJ(int sirues) {
        Query query = em.createNamedQuery("Elev.findByPJ");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Elev> findByCZE(int sirues) {
        Query query = em.createNamedQuery("Elev.findByCZE");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }

    @Override
    public List<Elev> findElevFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select e from Elev e";


        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();


        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where e." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and e." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        if (sortField != null) {
            if (sortOrder == SortOrder.ASCENDING) {
                queryString += " order by e." + sortField + " asc";
            }
            if (sortOrder == SortOrder.DESCENDING) {
                queryString += " order by e." + sortField + " desc";
            }
            queryString += ", e.nume asc, e.prenume asc, e.ini asc";

        } else {
            queryString += " order by e.nume asc, e.prenume asc, e.ini asc";
        }

        Query query = em.createQuery(queryString);
        query.setFirstResult(first);
        query.setMaxResults(pageSize);
        return query.getResultList();

        /*Query query = em.createQuery(queryString);
         return query.getResultList();*/
    }

    @Override
    public long countFilteredOrderedLimited(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filter) {

        String queryString = "select count(e) as nrElevi from Elev e";


        boolean hasFilter = false;
        Iterator<String> filterWalker = filter.keySet().iterator();


        while (filterWalker.hasNext()) {
            String filterProperty = filterWalker.next();
            String filterValue = String.valueOf(filter.get(filterProperty));

            if (filterValue != null) {
                if (!hasFilter) {
                    queryString += " where e." + filterProperty + " like '%" + filterValue + "%'";
                    hasFilter = true;
                } else {
                    queryString += " and e." + filterProperty + " like '%" + filterValue + "%'";
                }
            }

        }

        Query query = em.createQuery(queryString);
        return (Long) query.getSingleResult();
    }

    @Override
    public Object[] getRaportPrezenta(int disciplinaType, int groupType, int sirues) {
        String queryString;
        String discip = "";
        String filters = "";

        //1-rom, 2-mag, 3-mat, 4-total
        if (disciplinaType == 1) {
            discip = "rom";
        } else if (disciplinaType == 2) {
            discip = "mag";
            filters = "AND e.sectia_id=1 ";
        } else if (disciplinaType == 3) {
            discip = "mat";
        } else if (disciplinaType == 4) {
            discip = "final";
        }

        //0-unitate 1-CC 2-ISJ
        if (groupType == 0) {
            filters += "AND us.sirues=" + sirues + " ";
        } else if (groupType == 1) {
            filters += "AND cc.sirues=" + sirues + " ";
        }

        queryString = "SELECT count(*) as Total, SUM(IF(p." + discip + "=1,1,0)) as Prezenti, SUM(IF(e.sectia_id=2,IF(p." + discip + "=1,1,0),0)) as SectiaRomana, SUM(IF(e.sectia_id=1,IF(p." + discip + "=1,1,0),0)) as SectiaMaghiara, SUM(IF(p." + discip + "=1,IF(MOD(left(e.cnp,1),2)=1,1,0),0)) as Baieti, SUM(IF(p." + discip + "=1,IF(MOD(left(e.cnp,1),2)=0,1,0),0)) as Fete, SUM(IF(p." + discip + "=2,1,0)) as Neprezentati, SUM(IF(p." + discip + "=3,1,0)) as Eliminati FROM elev e LEFT JOIN media58 m ON e.media58_id=m.id LEFT JOIN unitate u ON e.unitate=u.sirues  LEFT JOIN unitate us ON u.unitate_superioara=us.sirues LEFT JOIN unitate cc ON us.centru_comunicare=cc.sirues LEFT JOIN prezenta p ON e.prezenta_id=p.id WHERE m.inscris=true " + filters;

        Query query = em.createNativeQuery(queryString);


        return (Object[]) query.getSingleResult();
    }

    @Override
    public List<Object[]> getRaportRezultate(int disciplinaType, int groupType, int sirues) {
        String queryString;
        String group1 = "";
        String grouping = "";
        String discip = "";
        String filters = "";
        String nota = "";

        //1-rom, 2-mag, 3-mat, 4-total
        if (disciplinaType == 1) {
            discip = "rom";
            nota = discip;
        } else if (disciplinaType == 2) {
            discip = "mag";
            nota = discip;
            filters = "AND e.sectia_id=1 ";
        } else if (disciplinaType == 3) {
            discip = "mat";
            nota = discip;
        } else if (disciplinaType == 4) {
            discip = "final";
            nota = "media";
        }

        //0-unitate 1-CE 2-ISJ
        if (groupType == 1) {
            filters += "AND ce.sirues=" + sirues + " ";
            group1 = "us.nume";
        } else if (groupType == 2) {
            group1 = "ce.nume";
        }
        grouping += " GROUP BY " + group1;

        queryString = "SELECT ifnull(" + group1 + ",\"Total\") as Unitate,"
                + " count(*) as Inscris,"
                + " SUM(IF(p." + discip + "=2,1,0)) as Neprezentati,"
                + " SUM(IF(p." + discip + "=3,1,0)) as Eliminati,"
                + " SUM(IF(p." + discip + "=1,1,0)) as Prezenti,"
                + " SUM(IF(n." + nota + "=0,IF(p." + discip + "=1,1,0),0)) as FaraNota,"
                + " SUM(IF(FLOOR(n." + nota + ")=1,1,0)) as t1,"
                + " SUM(IF(FLOOR(n." + nota + ")=2,1,0)) as t2,"
                + " SUM(IF(FLOOR(n." + nota + ")=3,1,0)) as t3,"
                + " SUM(IF(FLOOR(n." + nota + ")=4,1,0)) as t4,"
                + " SUM(IF(FLOOR(n." + nota + ")=5,1,0)) as t5,"
                + " SUM(IF(FLOOR(n." + nota + ")=6,1,0)) as t6,"
                + " SUM(IF(FLOOR(n." + nota + ")=7,1,0)) as t7,"
                + " SUM(IF(FLOOR(n." + nota + ")=8,1,0)) as t8,"
                + " SUM(IF(FLOOR(n." + nota + ")=9,1,0)) as t9,"
                + " SUM(IF(FLOOR(n." + nota + ")=10,1,0)) as t10"
                + " FROM elev e LEFT JOIN media58 m ON e.media58_id=m.id LEFT JOIN unitate u ON e.unitate=u.sirues  LEFT JOIN unitate us ON u.unitate_superioara=us.sirues LEFT JOIN unitate ce ON us.centru_evaluare=ce.sirues LEFT JOIN prezenta p ON e.prezenta_id=p.id LEFT JOIN nota_ic n ON n.id=e.nota_ic_id WHERE m.inscris=true " + filters + grouping + " WITH ROLLUP";

        Query query = em.createNativeQuery(queryString);

        return query.getResultList();
    }

    public List<Object[]> getRaportCNEE(int disciplinaType, int groupType, boolean dupa_contestatii) {
        String queryString;
        String group1 = "";
        String grouping = "";
        String discip = "";
        String filters = "";
        String nota = "";
        String contes = "nota";

        if (!dupa_contestatii) {
            contes += "_ic";
        }

        //1-rom, 2-mag, 3-mat, 4-total
        if (disciplinaType == 1) {
            discip = "rom";
            nota = discip;
        } else if (disciplinaType == 2) {
            discip = "mag";
            nota = discip;
            filters = "AND e.sectia_id=1 ";
        } else if (disciplinaType == 3) {
            discip = "mat";
            nota = discip;
        } else if (disciplinaType == 4) {
            discip = "final";
            nota = "media";
        }

        //0-total 1-mediu 2-sex
        if (groupType == 0) {
            group1 = "1";
            grouping += " GROUP BY " + group1;
        } else if (groupType == 1) {
            group1 = "x.nume";
            grouping += " GROUP BY " + group1;
        } else if (groupType == 2) {
            group1 = "IF(MOD(LEFT(e.cnp,1),2)=1,'Băieți','Fete')";
            grouping += " GROUP BY MOD(LEFT(e.cnp,1),2)";
        }


        queryString = "SELECT ifnull(" + group1 + ",\"Total\") as Unitate,"
                + " count(*) as Inscris,"
                + " SUM(IF(p." + discip + "=1,1,0)) as Prezenti,"
                + " SUM(IF(p." + discip + "=3,1,0)) as Eliminati,"
                + " SUM(IF(p." + discip + "=1,1,0)) as Prezenti,"
                + " SUM(IF(FLOOR(n." + nota + ")=1,1,0)) as t1,"
                + " SUM(IF(FLOOR(n." + nota + ")=2,1,0)) as t2,"
                + " SUM(IF(FLOOR(n." + nota + ")=3,1,0)) as t3,"
                + " SUM(IF(FLOOR(n." + nota + ")=4,1,0)) as t4,"
                + " SUM(IF(FLOOR(n." + nota + ")=5,1,0)) as t5,"
                + " SUM(IF(FLOOR(n." + nota + ")=6,1,0)) as t6,"
                + " SUM(IF(FLOOR(n." + nota + ")=7,1,0)) as t7,"
                + " SUM(IF(FLOOR(n." + nota + ")=8,1,0)) as t8,"
                + " SUM(IF(FLOOR(n." + nota + ")=9,1,0)) as t9,"
                + " SUM(IF(FLOOR(n." + nota + ")=10,1,0)) as t10"
                + " FROM elev e LEFT JOIN media58 m ON e.media58_id=m.id LEFT JOIN prezenta p ON e.prezenta_id=p.id LEFT JOIN " + contes + " n ON n.id=e." + contes + "_id LEFT JOIN mediu x ON x.id=e.mediu_id WHERE m.inscris=true " + filters + grouping;

        Query query = em.createNativeQuery(queryString);

        return query.getResultList();
    }

    @Override
    public List<RaportInscriereDataModel> getRaportInscriere(int sirues) {
        String queryString;
        if (sirues == 0) {
            queryString = "select ifnull(us.nume,\"TOTAL\") as Unitatea,  count(*) as Total,  sum(if (e.seria_id=1,1,0)) As Curenta,  sum(if (m.stare_id=7,1,0)) As Transfer,  sum(if (m.stare_id=4,1,0)) As Repetenti,  sum(if (m.stare_id=3,1,0)) As Corigenti,  sum(if (m.stare_id=5,1,0)) As Neincheiat,  sum(if (m.stare_id=6,1,0)) As Abandon,  sum(if (m.stare_id=2,if (e.seria_id=1,1,0),0)) As Promovati_curent,  sum(if (m.stare_id=2,if (e.seria_id=1,if (m.inscris=true,1,0),0),0)) As Promovati_curent_inscris,  sum(if (m.stare_id=2,if (e.seria_id=1,if (m.inscris=false,1,0),0),0)) As Promovati_curent_neinscris,  sum(if (e.seria_id=2,if (m.inscris=true,1,0),0)) As Anterior_inscris,  sum(if (m.stare_id=1,1,0)) As Necompletat from elev e  LEFT JOIN media58 m ON e.media58_id=m.id  LEFT JOIN unitate u ON e.unitate=u.sirues  LEFT JOIN unitate us ON u.unitate_superioara=us.sirues group by us.nume with rollup";
        } else {
            queryString = "select ifnull(u.nume,\"TOTAL\") as Unitatea, count(*) as Total,  sum(if (e.seria_id=1,1,0)) As Curenta,  sum(if (m.stare_id=7,1,0)) As Transfer,  sum(if (m.stare_id=4,1,0)) As Repetenti,  sum(if (m.stare_id=3,1,0)) As Corigenti,  sum(if (m.stare_id=5,1,0)) As Neincheiat,  sum(if (m.stare_id=6,1,0)) As Abandon,  sum(if (m.stare_id=2,if (e.seria_id=1,1,0),0)) As Promovati_curent,  sum(if (m.stare_id=2,if (e.seria_id=1,if (m.inscris=true,1,0),0),0)) As Promovati_curent_inscris,  sum(if (m.stare_id=2,if (e.seria_id=1,if (m.inscris=false,1,0),0),0)) As Promovati_curent_neinscris,  sum(if (e.seria_id=2,if (m.inscris=true,1,0),0)) As Anterior_inscris,  sum(if (m.stare_id=1,1,0)) As Necompletat from elev e  LEFT JOIN media58 m ON e.media58_id=m.id  LEFT JOIN unitate u ON e.unitate=u.sirues  LEFT JOIN unitate us ON u.unitate_superioara=us.sirues WHERE us.sirues=" + sirues + " group by u.nume with rollup";
        }
        Query query = em.createNativeQuery(queryString);
        List<Object> resultList = query.getResultList();
        List<RaportInscriereDataModel> raportList = new ArrayList<>();
        Iterator<Object> resultIterator = resultList.iterator();
        while (resultIterator.hasNext()) {
            Object[] result = (Object[]) resultIterator.next();
            RaportInscriereDataModel raport = new RaportInscriereDataModel();
            raport.setUnitate((String) result[0]);
            raport.setTotal((Long) result[1]);
            raport.setCurent(((BigDecimal) result[2]).longValue());
            raport.setTransfer(((BigDecimal) result[3]).longValue());
            raport.setRepetenti(((BigDecimal) result[4]).longValue());
            raport.setCorigenti(((BigDecimal) result[5]).longValue());
            raport.setNeincheiat(((BigDecimal) result[6]).longValue());
            raport.setAbandon(((BigDecimal) result[7]).longValue());
            raport.setPromovati(((BigDecimal) result[8]).longValue());
            raport.setPromoInscrisi(((BigDecimal) result[9]).longValue());
            raport.setPromoNeinscrisi(((BigDecimal) result[10]).longValue());
            raport.setAnterior(((BigDecimal) result[11]).longValue());
            raport.setNecompletat(((BigDecimal) result[12]).longValue());
            raportList.add(raport);
        }

        return raportList;
    }
}
