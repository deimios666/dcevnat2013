/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dcevnat2013.entity.NotaDc;

/**
 *
 * @author deimios
 */
@Stateless
public class NotaDcFacade extends AbstractFacade<NotaDc> implements NotaDcFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotaDcFacade() {
        super(NotaDc.class);
    }
    
}
