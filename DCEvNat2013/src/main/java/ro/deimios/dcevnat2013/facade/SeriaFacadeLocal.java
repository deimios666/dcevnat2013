/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Seria;

/**
 *
 * @author deimios
 */
@Local
public interface SeriaFacadeLocal {

    void create(Seria seria);

    void edit(Seria seria);

    void remove(Seria seria);

    Seria find(Object id);

    List<Seria> findAll();

    List<Seria> findRange(int[] range);

    int count();

    public Seria findByNume(String nume);
    
}
