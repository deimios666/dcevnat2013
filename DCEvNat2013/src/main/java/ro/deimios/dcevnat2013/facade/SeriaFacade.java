/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.Seria;

/**
 *
 * @author deimios
 */
@Stateless
public class SeriaFacade extends AbstractFacade<Seria> implements SeriaFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SeriaFacade() {
        super(Seria.class);
    }

    @Override
    public Seria findByNume(String nume) {
        Query query = em.createNamedQuery("Seria.findByNume");
        query.setParameter("nume", nume);
        return (Seria) query.getSingleResult();
    }
}
