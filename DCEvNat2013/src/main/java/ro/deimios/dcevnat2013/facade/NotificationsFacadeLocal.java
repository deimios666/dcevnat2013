/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Notifications;

/**
 *
 * @author deimios
 */
@Local
public interface NotificationsFacadeLocal {

    void create(Notifications notifications);

    void edit(Notifications notifications);

    void remove(Notifications notifications);

    Notifications find(Object id);

    List<Notifications> findAll();

    List<Notifications> findRange(int[] range);

    int count();

    public List<Notifications> findBySirues(int sirues);
    
}
