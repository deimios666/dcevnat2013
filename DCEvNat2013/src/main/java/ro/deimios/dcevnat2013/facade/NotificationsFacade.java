/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.Notifications;

/**
 *
 * @author deimios
 */
@Stateless
public class NotificationsFacade extends AbstractFacade<Notifications> implements NotificationsFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotificationsFacade() {
        super(Notifications.class);
    }

    public List<Notifications> findBySirues(int sirues) {
        Query query = em.createNamedQuery("Notifications.findBySirues");
        query.setParameter("sirues", sirues);
        return query.getResultList();
    }
}
