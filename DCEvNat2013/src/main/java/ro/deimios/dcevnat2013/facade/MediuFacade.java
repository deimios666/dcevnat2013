/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.Mediu;

/**
 *
 * @author deimios
 */
@Stateless
public class MediuFacade extends AbstractFacade<Mediu> implements MediuFacadeLocal {

    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MediuFacade() {
        super(Mediu.class);
    }

    @Override
    public Mediu findByNume(String nume) {
        Query query = em.createNamedQuery("Mediu.findByNume");
        query.setParameter("nume", nume);
        return (Mediu) query.getSingleResult();
    }
}
