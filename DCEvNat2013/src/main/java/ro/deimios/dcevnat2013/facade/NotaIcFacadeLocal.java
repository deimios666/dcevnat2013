/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.NotaIc;

/**
 *
 * @author deimios
 */
@Local
public interface NotaIcFacadeLocal {

    void create(NotaIc notaIc);

    void edit(NotaIc notaIc);

    void remove(NotaIc notaIc);

    NotaIc find(Object id);

    List<NotaIc> findAll();

    List<NotaIc> findRange(int[] range);

    int count();
    
}
