/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Mediu;

/**
 *
 * @author deimios
 */
@Local
public interface MediuFacadeLocal {

    void create(Mediu mediu);

    void edit(Mediu mediu);

    void remove(Mediu mediu);

    Mediu find(Object id);

    List<Mediu> findAll();

    List<Mediu> findRange(int[] range);

    int count();

    public Mediu findByNume(String nume);
    
}
