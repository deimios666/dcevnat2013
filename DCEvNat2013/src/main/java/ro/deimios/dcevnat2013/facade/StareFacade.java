/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.Stare;

/**
 *
 * @author deimios
 */
@Stateless
public class StareFacade extends AbstractFacade<Stare> implements StareFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StareFacade() {
        super(Stare.class);
    }
    
     @Override
    public Stare findByNume(String nume) {
        Query query = em.createNamedQuery("Stare.findByNume");
        query.setParameter("nume", nume);
        return (Stare) query.getSingleResult();
    }
    
}
