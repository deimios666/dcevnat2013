/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Stare;

/**
 *
 * @author deimios
 */
@Local
public interface StareFacadeLocal {

    void create(Stare stare);

    void edit(Stare stare);

    void remove(Stare stare);

    Stare find(Object id);

    List<Stare> findAll();

    List<Stare> findRange(int[] range);

    int count();

    public Stare findByNume(String nume);
    
}
