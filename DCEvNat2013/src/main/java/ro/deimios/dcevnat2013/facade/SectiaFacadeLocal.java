/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.Sectia;

/**
 *
 * @author deimios
 */
@Local
public interface SectiaFacadeLocal {

    void create(Sectia sectia);

    void edit(Sectia sectia);

    void remove(Sectia sectia);

    Sectia find(Object id);

    List<Sectia> findAll();

    List<Sectia> findRange(int[] range);

    int count();

    public Sectia findByNume(String nume);
    
}
