/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ro.deimios.dcevnat2013.entity.Transferlog;

/**
 *
 * @author deimios
 */
@Stateless
public class TransferlogFacade extends AbstractFacade<Transferlog> implements TransferlogFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransferlogFacade() {
        super(Transferlog.class);
    }
    
}
