/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.StarePrezenta;

/**
 *
 * @author deimios
 */
@Local
public interface StarePrezentaFacadeLocal {

    void create(StarePrezenta starePrezenta);

    void edit(StarePrezenta starePrezenta);

    void remove(StarePrezenta starePrezenta);

    StarePrezenta find(Object id);

    List<StarePrezenta> findAll();

    List<StarePrezenta> findRange(int[] range);

    int count();

    public StarePrezenta findByNume(String nume);
    
}
