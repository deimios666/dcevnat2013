/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ro.deimios.dcevnat2013.entity.StarePrezenta;

/**
 *
 * @author deimios
 */
@Stateless
public class StarePrezentaFacade extends AbstractFacade<StarePrezenta> implements StarePrezentaFacadeLocal {
    @PersistenceContext(unitName = "DCEvNatPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StarePrezentaFacade() {
        super(StarePrezenta.class);
    }
    
    @Override
        public StarePrezenta findByNume(String nume) {
        Query query = em.createNamedQuery("StarePrezenta.findByNume");
        query.setParameter("nume", nume);
        return (StarePrezenta) query.getSingleResult();
    }
    
    
}
