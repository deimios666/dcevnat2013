/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.facade;

import java.util.List;
import javax.ejb.Local;
import ro.deimios.dcevnat2013.entity.NotaDc;

/**
 *
 * @author deimios
 */
@Local
public interface NotaDcFacadeLocal {

    void create(NotaDc notaDc);

    void edit(NotaDc notaDc);

    void remove(NotaDc notaDc);

    NotaDc find(Object id);

    List<NotaDc> findAll();

    List<NotaDc> findRange(int[] range);

    int count();
    
}
