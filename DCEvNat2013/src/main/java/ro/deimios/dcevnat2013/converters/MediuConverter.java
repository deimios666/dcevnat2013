package ro.deimios.dcevnat2013.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dcevnat2013.entity.Mediu;
import ro.deimios.dcevnat2013.facade.MediuFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(forClass = ro.deimios.dcevnat2013.entity.Mediu.class, value = "mediuConverter")
public class MediuConverter implements Converter {

    MediuFacadeLocal mediuFacade = lookupMediuFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return mediuFacade.findByNume(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Mediu) value).getNume());
    }

    private MediuFacadeLocal lookupMediuFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (MediuFacadeLocal) c.lookup("java:global/DCEvNat/MediuFacade!ro.deimios.dcevnat2013.facade.MediuFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
