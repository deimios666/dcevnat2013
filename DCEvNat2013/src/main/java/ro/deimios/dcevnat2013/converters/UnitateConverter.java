package ro.deimios.dcevnat2013.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dcevnat2013.entity.Unitate;
import ro.deimios.dcevnat2013.facade.UnitateFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(forClass = ro.deimios.dcevnat2013.entity.Unitate.class, value = "unitateConverter")
public class UnitateConverter implements Converter {

    UnitateFacadeLocal unitateFacade = lookupUnitateFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return unitateFacade.findByNume(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Unitate) value).getNume());
    }

    private UnitateFacadeLocal lookupUnitateFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (UnitateFacadeLocal) c.lookup("java:global/DCEvNat/UnitateFacade!ro.deimios.dcevnat2013.facade.UnitateFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
