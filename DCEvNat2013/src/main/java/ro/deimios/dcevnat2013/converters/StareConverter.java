package ro.deimios.dcevnat2013.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dcevnat2013.entity.Stare;
import ro.deimios.dcevnat2013.facade.StareFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(forClass = ro.deimios.dcevnat2013.entity.Stare.class, value = "stareConverter")
public class StareConverter implements Converter {

    StareFacadeLocal stareFacade = lookupStareFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return stareFacade.findByNume(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Stare) value).getNume());
    }

    private StareFacadeLocal lookupStareFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (StareFacadeLocal) c.lookup("java:global/DCEvNat/StareFacade!ro.deimios.dcevnat2013.facade.StareFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
