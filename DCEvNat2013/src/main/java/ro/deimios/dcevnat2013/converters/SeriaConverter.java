package ro.deimios.dcevnat2013.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dcevnat2013.entity.Seria;
import ro.deimios.dcevnat2013.facade.SeriaFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(forClass = ro.deimios.dcevnat2013.entity.Seria.class, value = "seriaConverter")
public class SeriaConverter implements Converter {

    SeriaFacadeLocal seriaFacade = lookupSeriaFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return seriaFacade.findByNume(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Seria) value).getNume());
    }

    private SeriaFacadeLocal lookupSeriaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (SeriaFacadeLocal) c.lookup("java:global/DCEvNat/SeriaFacade!ro.deimios.dcevnat2013.facade.SeriaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
