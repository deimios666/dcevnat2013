package ro.deimios.dcevnat2013.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import ro.deimios.dcevnat2013.entity.Mediu;

/**
 *
 * @author deimios
 */
@FacesConverter("booleanConverter")
public class BooleanConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return value.equals("Da");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        if ((boolean) value) {
            return "Da";
        } else {
            return "Nu";
        }
    }
}
