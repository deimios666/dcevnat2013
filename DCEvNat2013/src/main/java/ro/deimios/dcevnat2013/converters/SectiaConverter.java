package ro.deimios.dcevnat2013.converters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ro.deimios.dcevnat2013.entity.Sectia;
import ro.deimios.dcevnat2013.facade.SectiaFacadeLocal;

/**
 *
 * @author deimios
 */
@FacesConverter(forClass = ro.deimios.dcevnat2013.entity.Sectia.class, value = "sectiaConverter")
public class SectiaConverter implements Converter {

    SectiaFacadeLocal sectiaFacade = lookupSectiaFacadeLocal();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return sectiaFacade.findByNume(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Sectia) value).getNume());
    }

    private SectiaFacadeLocal lookupSectiaFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (SectiaFacadeLocal) c.lookup("java:global/DCEvNat/SectiaFacade!ro.deimios.dcevnat2013.facade.SectiaFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
