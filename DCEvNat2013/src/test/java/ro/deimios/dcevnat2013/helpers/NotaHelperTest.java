/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.deimios.dcevnat2013.helpers;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author deimios
 */
public class NotaHelperTest {

    public NotaHelperTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of contestat method, of class NotaHelper.
     */
    @org.junit.Test
    public void testContestat() {
        System.out.println("contestat");
        BigDecimal ic0 = BigDecimal.ZERO;
        BigDecimal dc0 = BigDecimal.ZERO;
        BigDecimal ic500 = new BigDecimal("5.00");
        BigDecimal dc500 = new BigDecimal("5.00");
        BigDecimal dc549 = new BigDecimal("5.49");
        BigDecimal dc550 = new BigDecimal("5.50");
        BigDecimal ic949 = new BigDecimal("9.49");
        BigDecimal ic950 = new BigDecimal("9.50");
        BigDecimal dc951 = new BigDecimal("9.51");

        //if any nota=0 result=the other
        assertEquals(ic0, NotaHelper.contestat(ic0, dc0));
        assertEquals(dc500, NotaHelper.contestat(ic0, dc500));
        assertEquals(ic500, NotaHelper.contestat(ic500, dc0));

        //if they are equal, IC returned
        assertEquals(ic500, NotaHelper.contestat(ic500, dc500));

        //if diff is>0.50
        assertEquals(ic500, NotaHelper.contestat(ic500, dc549));
        assertEquals(dc550, NotaHelper.contestat(ic500, dc550));

        //if IC>=9.50 then automatically accept DC
        assertEquals(ic949, NotaHelper.contestat(ic949, dc951));
        assertEquals(dc951, NotaHelper.contestat(ic950, dc951));

    }

    /**
     * Test of average method, of class NotaHelper.
     */
    @org.junit.Test
    public void testAverage_3args() {
        System.out.println("average");
        BigDecimal rom0 = BigDecimal.ZERO;
        BigDecimal mag0 = BigDecimal.ZERO;
        BigDecimal mat0 = BigDecimal.ZERO;
        BigDecimal rom100 = BigDecimal.ONE;
        BigDecimal mag100 = BigDecimal.ONE;
        BigDecimal mat100 = BigDecimal.ONE;
        BigDecimal rom500 = new BigDecimal("5.00");
        BigDecimal mag500 = new BigDecimal("5.00");
        BigDecimal mat500 = new BigDecimal("5.00");
        BigDecimal rom1000 = BigDecimal.TEN;
        BigDecimal mag1000 = BigDecimal.TEN;
        BigDecimal mat1000 = BigDecimal.TEN;
        BigDecimal seven = new BigDecimal("7.00");
        BigDecimal f666 = new BigDecimal("6.66");
        BigDecimal f733 = new BigDecimal("7.33");

        //if any 0, result 0
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom0, mag0, mat0));
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom0, mag1000, mat1000));
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom1000, mag0, mat1000));
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom1000, mag1000, mat0));

        //test calculation
        assertEquals(seven, NotaHelper.average(rom100, mag1000, mat1000));
        assertEquals(seven, NotaHelper.average(rom1000, mag100, mat1000));
        assertEquals(seven, NotaHelper.average(rom1000, mag1000, mat100));

        //test rounding
        assertEquals(f666, NotaHelper.average(rom500, mag500, mat1000));
        assertEquals(f733, NotaHelper.average(rom500, seven, mat1000));

    }

    /**
     * Test of average method, of class NotaHelper.
     */
    @org.junit.Test
    public void testAverage_BigDecimal_BigDecimal() {
        System.out.println("average");
       BigDecimal rom0 = BigDecimal.ZERO;
        
        BigDecimal mat0 = BigDecimal.ZERO;
        BigDecimal rom100 = BigDecimal.ONE;
        
        BigDecimal mat100 = BigDecimal.ONE;
        BigDecimal rom500 = new BigDecimal("5.00");
        
        BigDecimal mat500 = new BigDecimal("5.00");
        BigDecimal rom1000 = BigDecimal.TEN;
        
        BigDecimal mat1000 = BigDecimal.TEN;
        BigDecimal seven = new BigDecimal("7.00");
        BigDecimal f750 = new BigDecimal("7.50");
        BigDecimal f666 = new BigDecimal("6.66");
        BigDecimal f699 = new BigDecimal("6.99");
        BigDecimal f733 = new BigDecimal("7.33");
        BigDecimal f1000 = new BigDecimal("10.00");

        //if any 0, result 0
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom0, mat0));
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom0, mat1000));
        assertEquals(BigDecimal.ZERO, NotaHelper.average(rom1000,mat0));

        //test calculation
        assertEquals(f1000, NotaHelper.average(rom1000,  mat1000));
        assertEquals(f750, NotaHelper.average(rom500, mat1000));

        //test rounding
        assertEquals(f699, NotaHelper.average(f666, f733));
    }
}